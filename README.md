# MuStARD - M*achine Learning* S*ystem for* A*utomated* R*NA* D*iscovery*

## Installation

If you want to run MuStARD on GPU, you have to install NVIDIA GPU drivers, the Cuda toolkit, and cuDNN.
Versions have to be compatible with the tensorflow version that you want to use.

1. Download and install [anaconda](https://www.anaconda.com/distribution/) for python 2.7 and Linux

1. Create and activate a conda environment with python 2.7

   `conda create -n py27 python=2.7 anaconda`  
   `conda activate py27`

1. Optional - Install R and ggplot2 library if these are not already installed on your system

   `conda install -c r r`  
   `conda install -c r r-ggplot2`

1. Install perl and the following modules

   `conda install -c anaconda perl`  
   `conda install -c bioconda perl-set-intervaltree`  
   `conda install -c bioconda perl-parallel-forkmanager`  
   `conda install -c bioconda perl-io-gzip`

1. Install Vienna RNA package and Bedtools

   `conda install -c bioconda viennarna`  
   `conda install -c bioconda bedtools`

1. Install tensorflow and Keras 

   `pip install tensorflow-gpu`  
   `pip install keras`

## MuStARD modules

MuStARD provides two basic modules and several utility scripts.

* `MuStARD train` provides the means to train new models on Biological problems related to patterns embedded in DNA/RNA sequences.
* `MuStARD predict` utilizes a previously trained model to scan arbitrarily long genomic regions for elements that belong in the under-study categories/classes.
* In `src/utilities/perl/` there are several perl scripts that can be utilized to further process the results of `MuStARD predict` module.

A detailed documentation of MuStARD modules and utility scripts is provided in the sections that follow. Several use-cases for MuStARD are also presented.

## Training a new model

MuStARD's training module has minimal input requirements. In general, the user only needs to provide genomic regions representing different classes in different bed 
formatted files and the corresponding genome file in fasta format. In case conservation will be included in the training process, the user needs to also provide 
a directory with the phyloP basewise conservation score in wig format.

In example:  

`./MuStARD.pl train --class 1,2 --maxSize 100 --reinfNum 5 --extFlag 1 --shufClassFlag 0 --inputMode sequence,RNAfold,conservation --modelType CNN --exclTest chr14
--exclValid chr2,chr3 --list positives.bed,negatives.bed --genome genome.fa --consDir conservation_dir --dir output_dir`

**Mandatory arguments**  

* `--list positives.bed,negatives.bed`  

  This argument takes a comma delimited list of bed formatted files corresponding to the genomic intervals of each category/class. The order of the files is important
  and it is combined with the `--class` option that is explained below. The bed formatted files are required to include at least 6 fields. For example:  
  
  `chr1 168900  168980  sample_1    0   +`  
  `chrX 289980  290080  sample_2    0   -`

* `--genome genome.fa`  

  This argument receives a fasta formatted file corresponding to the genome assembly that the intervals in `--list` were derived.  

* `--consDir conservation_dir`  

  In this argument we specify the directory hosting phyloP basewise conservation score in wig format. The directory should include one gzipped file per chromosome. 
  The naming of files should be `chr*.wigFix.gz`, were * represents the chromosome number/letter.  

* `--dir output_dir`  

  This argument expects the directory where intermediate training files will be placed as well as the trained model files and performance.

**Optional arguments**

* `--class 1,2`  

  This argument expects a comma delimited list of class assignments for the files provided in the `--list` argument. These two arguments are combined in a way that the 
  order of the elements (separated by comma) across both lists is matched. In this particular example, file `positives.bed` is assigned to class `1` and file 
  `negatives.bed` to class `2`.  

* `--maxSize 100`  

  This argument controls the maximum size of the input samples. For this particular example, every genomic interval with size > 100nt will be discarded.

* `--extFlag 1`  

  This argument controls whether genomic intervals less than `--maxSize` will be extended to `--maxSize` using the real genomic neighbourhood or whether they will 
  be padded with `N`. In our experience, using the default setting (`--extFlag 1`) always leads to more robust models.

* `--reinfNum 5`  

  With this argument we can specify the number of *replicates* that will be created for each sample. For each *replicate* the original genomic interval remains 
  the same but it is randomly placed within the `--maxSize` space. For example (given `--maxSize 100` and `--extFlag 1`):  
  
  Original interval: `chr1   100 160 sample_1    0   +`  
  Original interval after extention to `--maxSize 100`: `chr1   80 180 sample_1    0   +`  
  *Replicate 1*: `chr1   70 170 sample_1_reinf_1    0   +`  
  *Replicate 2*: `chr1   95 195 sample_1_reinf_2    0   +`  
  *Replicate 3*: `chr1   62 162 sample_1_reinf_3    0   +`  
  ...
  
  In case of `--extFlag 0` the same procedure is applied using `N` to pad the sequences.

* `--shufClassFlag 0`  

  This argument controls whether an additional class will be created (the random class) by generating 1 random sequence (using MEME's 
  [fasta-shuffle-letters](http://meme-suite.org/doc/fasta-shuffle-letters.html)) for each sample provided in `--list`. This setting is turned off by default.

* `--inputMode sequence,RNAfold,conservation`  

  This argument requires a comma separated list of code parameters that control the number of Convolutional branches that will be created. The branch that 
  will process raw sequences is specified as `sequence`, the secondary structure as `RNAfold` and evolutionary conservation as `conservation`. The order of 
  code words is irrelevant.

* `--modelType CNN`  

  This argument specifies the type of architecture to build. For the time being, only Convolutional Neural Networks (CNN) is supported. Other types of architectures 
  such as Convolutional Autoencoders (CAE) or Convolutional Variational Autoencoders (CVAE) are currently under development.

* `--exclTest chr14`  

  Comma delimited list that controls the chromosomes' samples that will be kept out of the training process for testing the model after all training epochs 
  are concluded.

* `--exclValid chr2,chr3`  

  Comma delimited list that specifies the chromosomes' samples that will be used for validating the performance of the model at the end of each epoch.

**Output directory structure**

As stated above all intermediate files and final model produced by `MuStARD train` will be placed in `--dir` directory. In the current example, the 
results will be placed in `--dir output_dir`. Two subdirectories will be created:

* `output_dir/Data/`  

  All intermediate files, properly formatted to be further utilized by `MuStARD train`, will be placed in this subdirectory. Three prefixes are used to organize 
  the files, `train.*`, `test.*` and `validation.*`, where `*` represents various intermediate processes such as RNAfold results, conservation vectors etc. The 
  final files that are subsequently loaded as numpy arrays according to Keras specifications are for example `train.sequence.tab.gz`, `train.RNAfold.tab.gz`, 
  `train.conservation.tab.gz`.

* `output_dir/Models/`  

  The final model(s) will be placed in this directory. More specifically, for each combination of hyperparameters, as specified in the grid search block in 
  `src/utilities/python/train_CNN.py` (lines 75-78), a subdirectory named after the hyperparameter combination 
  (i.e. `output_dir/Models/batch256_dropout0.2_lr0.0001_filters40/`) will be created. In this subdirectory, four files will be created. The final model in 
  hdf5 format, a log file as well as two graphs with the performance of the model during the training process.

## Using a previously trained pre-miRNA detection model for scanning small RNAseq enriched regions (under development)

**Preparing input files**

We will describe the process for preparing small RNA-Seq data for being processed by MuStARD. In reality, our algorithm works on any genomic intervals in BED6
format. For small RNA-Seq datasets in particular, users can apply simple or more advanced methods to aggregate aligned reads into clusters (or peaks). For 
example, one could use the `bedtools merge` utility to collapse overlapping reads and then the `bedtools coverage` utility on the previous output, to calculate 
the amount of reads overlapping each merged genomic interval. Subsequently, the intervals can be sorted and then selected by the expression level, i.e. top 10% 
etc. The selected intervals have to be formatted according to BED6 standards, which specify 6-columns tab-delimited files that include chromosome, start and 
stop coordinates, interval id, score and +/- strand.

At this point we are ready to apply MuStARD using 2 distinct operating approaches, static or scanning mode. The parameter that controls the operating modes is 
`--staticPredFlag [0,1]`. When set at 0, MuStARD scans the genomic intervals and then creates hotspots of positive predictions. When set at 1, MuStARD will 
treat the input intervals as sequences that have to be evaluated as they are. If an interval's size is less than the size indicated by `--winSize` parameter, 
then it is discarded in the case of the scanning mode, while for the static mode, it will be expanded equally in both directions until its size fits the 
parameter value. Intervals with size larger than the size indicated by the `--winSize` parameter will be shrinked equally in both directions when static mode 
is enabled.

**Scanning mode**

The following command shows how we can apply MuStARD on a set of genomic intervals in a test_input.bed file.

`./MuStARD.pl predict --winSize 100 --staticPredFlag 0 --step 5 --modelDirName name_of_model --inputMode sequence,RNAfold,conservation --chromList chr14 
--targetIntervals test_input.bed --genome genome_file.fa --consDir PhyloP_conservation_directory 
--dir results_directory --model previously_trained_model.hdf5 --modelType CNN --classNum 2`

* `--modelDirName name_of_model`  

  The value of this parameter will be used to create a subdirectory based on the `--dir` value. In this particular example, the subdirectory will be 
  `results_directory/name_of_model`. All intermediate files as well as the final results will be placed here.

* `--inputMode sequence,RNAfold,conservation`  

  This parameter signifies the convolutional branches that have been used for training. The value has to be a comma delimited list containing sequence, RNAfold, 
  conservation, in any order or combination. However, it has to include the same branches used for training.

* `--chromList chr14`  

  This parameter can be used to filter out chromosomes from the input interval list that will be processed by MuStARD. If set to `all`, all intervals will be 
  processed. In the example above, only chromosome 14 will be accepted. The value can also be a comma delimited list depicting the valid chromosomes, in 
  example, `chr1,chr5` etc. The chromosome naming needs to be exactly the same as in the input interval BED6 file.

* `--targetIntervals test_input.bed`  

  This parameter specifies the list of intervals to be scanned. The requirements have been thoroughly discussed in the previous section.

* `--genome genome_file.fa`  

  This value holds the path to the genome file in fasta format.

* `--consDir PhyloP_conservation_directory`  

  In this argument we specify the directory hosting phyloP basewise conservation score in wig format. The directory should include one gzipped file per chromosome. 
  The naming of files should be `chr*.wigFix.gz`, were * represents the chromosome number/letter.

* `--model previously_trained_model.hdf5`  

  Path to the previously trained model in hdf5 format.

* `--modelType CNN`  

  This is the model type that was used for training. For the time being, the only accepted value is `CNN` with the architecture depicted in Figure 1 of the MuStARD
  manuscript.

* `--classNum 2`  

  The number of classes used for training.

* `--dir results_directory`  

  The directory that will hold all results. As explained above, a subdirectory will be created based on the combination of the `--dir` and `--modelDirName` values. 
  In this particular example, the subdirectory will be `results_directory/name_of_model`. In this folder, three subdirectories with intermediate files will be 
  generated. The `results_directory/name_of_model/intermediate_files` holds the raw MuStARD prediction scores as calculated by Keras. 
  The `results_directory/name_of_model/bedGraph_tracks` holds the gzipped BED6 and bedGraph formatted prediction results, in separated files per chromosome and 
  class. The naming convention is `predictions.chr*.class_*.bed.gz` and `predictions.chr*.class_*.bedGraph.gz`. If more than one chromosomes were processed, there 
  will be two additional files per class that contain all chromosome results, `all.predictions.class_*.bed.gz` and `all.predictions.class_*.bedGraph.gz`.

**Static mode (under development)**

## Applying `src/utilities/perl/` utility scripts for post-processing `MuStARD predict` results (under development)

**Generating hotspots of positive predictions after applying MuStARD's scanning mode**

After running `MuStARD predict` using scanning mode, predictions are placed under `results_directory/name_of_model/bedGraph_tracks` as described in a previous 
section. To generate clusters/hotspots of positive predictions users can apply the `call_hotspots_per_threshold.pl` utility. This script will generate hotspots 
of positive prediction for multiple thresholds, using 2 types of merging the overlapping positive predictions (with and without considering the strand). After 
navigating to MuStARD's installation directory, please execute the following command, if the positive class index is `0`:

`perl src/utilities/perl/call_hotspots_per_threshold.pl --minThresh 0.1 --maxThresh 0.9 --threshStep 0.02 --winSize 100 --predFile 
results_directory/name_of_model/bedGraph_tracks/bedGraph_tracks/all.predictions.class_0.bedGraph.gz --outDir results_directory/name_of_model/hotspots_per_threshold`

* `--minThresh 0.1`  

  Minimum score threshold for merging overlapping positive predictions and generating hotspots.

* `--maxThresh 0.9`  

  Maximum score threshold for merging overlapping positive predictions and generating hotspots.

* `--threshStep 0.02`  

  At each iteration, the score threshold will be incremented by this value and separate files per threshold will be generated.

* `--winSize 100`  

  Window size that will be used to merge overlapping positive predictions. For more accurate results, this value should match the window file used for training.

* `--predFile results_directory/name_of_model/bedGraph_tracks/bedGraph_tracks/all.predictions.class_0.bedGraph.gz`  

  The bedGraph formatted predictions for the specific class that we are interested in. 

* `--outDir results_directory/name_of_model/hotspots_per_threshold`  

  The directory that the BED6 formatted hotspots will be placed in. As described above, hotspots are generated with and without considering strand. As a result, 
  two files per threshold will be generated, `results_directory/name_of_model/hotspots_per_threshold/hotspots.stranded.threshold.*.bed` and 
  `results_directory/name_of_model/hotspots_per_threshold/hotspots.unstranded.threshold.*.bed`. Where `*` denotes the score at each iteration.

**Getting the secondary structure from MuStARD's scanning mode results**

For those that want to walk the extra mile by generating the secondary structure of MuStARD's hotspot predictions, the process is straightforward and can be 
summarized in the following steps. Let's assume that we want to focus on a conservative set of predictions based on `0.8` score threshold and the stranded method 
of creating hotspots.

* `mkdir results_directory/name_of_model/RNAfold && cd results_directory/name_of_model/RNAfold`  

  With this command, we can generate a subdirectory in the directory structure described in the example of previous sections, to hold the secondary structure 
  predictions. After creating the directory, with the command after `&&` we ensure that our terminal will point to the newly created directory.

* `bedtools getfasta -s -name -fi genome_file.fa -bed ../hotspots_per_threshold/hotspots.stranded.threshold.0.8.bed -fo hotspots.stranded.threshold.0.8.fa`  

  With this bedtools command we utilize the desired hotspots file to extract the corresponding sequences from the genome_file.fa and put them into 
  hotspots.stranded.threshold.0.8.fa. If we want to use the hotspots generated with the unstranded mode, then the `-s` option has to be removed.

* `RNAfold --jobs=2 --infile=hotspots.stranded.threshold.0.8.fa --outfile=hotspots.stranded.threshold.0.8.RNAfold`  

  This is a minimal, in terms of options meddling, call to the RNAfold utility for processing the sequences that were predicted by MuStARD as pre-miRNA hosts. 
  By default, RNAfold translates Ts to Us and generates postscript formatted secondary structure predictions, one file per sequence in the `--infile` parameter 
  value. All postscript files are placed in the terminal's current working directory. In the output file, specified by `--outfile` parameter, the secondary 
  structure in dot-parenthesis format is appended below the fasta formatted sequences from the input file. The number of CPUs used for folding is specified by 
  `--jobs` parameter.
