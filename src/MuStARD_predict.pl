use warnings;
use strict;

use Getopt::Long;
use FindBin;
use lib "$FindBin::Bin/lib/perl/";
# use Files::CleanUp;
use Files::Format;
use Models::DNN;

my $window_size = 100;
my $step_size = 5;
my $static_predict_flag = 0;
my $inputMode = "sequence";
my $modelDirName = "standardModel";
my $thread_num = 10;
my $interm_dir = "same";

my ($help, $chrom_list, $interval_file, $working_dir, $genome_file, $model_file, $class_num, $conservation_dir, $model_type);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'chromList=s' => \$chrom_list,
		'targetIntervals=s' => \$interval_file,
		'dir=s' => \$working_dir,
		'intermDir=s' => \$interm_dir,
		'winSize=i' => \$window_size,
		'staticPredFlag=i' => \$static_predict_flag,
		'classNum=i' => \$class_num,
		'inputMode=s' => \$inputMode,
		'step=i' => \$step_size,
		'model=s' => \$model_file,
		'genome=s' => \$genome_file,
		'consDir=s' => \$conservation_dir,
		'modelDirName=s' => \$modelDirName,
		'modelType=s' => \$model_type,
		'threads=s' => \$thread_num
	)
or defined $help or (!defined $chrom_list or !defined $interval_file or !defined $working_dir or !defined $genome_file or !defined $class_num or !defined $conservation_dir or !defined $model_type));

warn "\n\tMuStARD_predict in $working_dir with parameters:\n".
	"\t\tIntermediate directory - $interm_dir\n".
	"\t\tChrom list - $chrom_list\n".
	"\t\tInterval file - $interval_file\n".
	"\t\tGenome file - $genome_file\n".
	"\t\tConservation dir - $conservation_dir\n".
	"\t\tModel dir name - $modelDirName\n".
	"\t\tModel file - $model_file\n".
	"\t\tWindow size - $window_size\n".
	"\t\tStep size - $step_size\n".
	"\t\tInput mode - $inputMode\n".
	"\t\tModel type - $model_type\n".
	"\t\tThread number - $thread_num\n".
	"\t\tStatic prediction - $static_predict_flag\n\n";

system "mkdir $working_dir" unless -d $working_dir;
system "mkdir $working_dir/predict" unless -d "$working_dir/predict";

my $tmp_working_dir = $static_predict_flag == 1 ? "$working_dir/predict/static" : "$working_dir/predict/scan";
system "mkdir $tmp_working_dir" unless -d $tmp_working_dir;

$interm_dir = $tmp_working_dir if $interm_dir eq "same";

warn "\t\tScanning genome based on the interval files and extracting sequences.\n";
my $chrom_list_real = Files::Format::extract_target_sequences($chrom_list, $interval_file, $genome_file, $window_size, $step_size, $interm_dir, $static_predict_flag, $inputMode, $conservation_dir, $thread_num);

my $time = time;
warn "\t\tMaking predictions.\n";
Models::DNN::predict_targets($chrom_list_real, "$working_dir/predict", $interm_dir, "$FindBin::Bin/utilities/python", $model_file, $static_predict_flag, $class_num, $step_size, $inputMode, $modelDirName, $model_type);
warn "\t\t\tFinished in ".(time - $time)."\n";

sub usage {

	print	"\nUsage: MuStARD.pl predict [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--chromList chr1,chr2,...\t\t\tList of chromosomes to be scanned. If specified as all, every chromosome in the interval file will be used.\n".
		"\t--targetIntervals target/interval/file\t\tBed formatted file with intervals for static or scanned prediction.\n".
		"\t--genome /path/to/genome.fa\t\t\tGenome fasta file corresponding to the reference genome.\n".
		"\t--consDir /path/to/cons/dir\t\t\tDirectory with conservation files in wig.gz format. If inputMode doesn't include conservation, put NaN here.\n".
		"\t--dir working/dir/Jobs/job_...\t\t\tWorking directory for placing the results. It could be the directory used for training.\n".
		"\t--model path/to/model/file\t\t\tFile path of the trained model that will be used for the prediction process.\n".
		"\t--classNum INT\t\t\t\t\tNumber of classes that the model is able to recognize.\n".
		"\t--modelType [CNN|CAE|CVAE]\t\t\tType of architecture/model that has been used for training.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\t\t\tPrint this help message.\n".
		"\t--modelDirName STR\t\t\t\tName of the model subdirectory that prediction results will be placed into. Default = standardModel.\n".
		"\t--intermDir /path/to/preprocess/dir\t\tPath of the directory that pre-processed files needed to be calculated once will be placed. Default is same as --dir.\n".
		"\t--winSize INT\t\t\t\t\tWindow size that will be used for the prediction. Must be equal to the corresponding size used for training. Default = 100.\n".
		"\t--staticPredFlag [1,0]\t\t\t\tFlag for activating static prediction of the entries in interval file. Default = 0 (scanning is activated).\n".
		"\t--inputMode [sequence|RNAfold|conservation]\tComma separated list of input data. Could be sequence,RNAfold or RNAfold,sequence or sequence,RNAfold,conservation etc. Default = sequence.\n".
		"\t--threads INT\t\t\t\t\tNumber of threads to use for pre-processing. Default = 10.\n".
		"\t--step INT\t\t\t\t\tStep of the scanning window. Default = 5.\n";
	exit;
}
