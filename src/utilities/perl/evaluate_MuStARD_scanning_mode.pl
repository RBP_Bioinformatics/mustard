use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $window_size = 100;
my $overlap_fraction = 0.1;

my ($help, $prediction_dir, $working_dir, $annotation_file, $class_index);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predDir=s' => \$prediction_dir,
		'outDir=s' => \$working_dir,
		'anotFile=s' => \$annotation_file,
		'classIndex=i' => \$class_index,
		'winSize=i' => \$window_size,
		'overlapFraction=f' => \$overlap_fraction
	)
or defined $help or (!defined $prediction_dir or !defined $working_dir or !defined $annotation_file or !defined $class_index));

warn "\n\tevaluate_MuStARD_scanning_mode in $working_dir with parameters:\n".
	"\t\tPrediction dir - $prediction_dir\n".
	"\t\tAnnotation file - $annotation_file\n".
	"\t\tClass index - $class_index\n".
	"\t\tWindow size - $window_size\n".
	"\t\tOverlap fraction - $overlap_fraction\n\n";

system "mkdir $working_dir" unless -d $working_dir;

my $predictions = merge_nearby_predictions($prediction_dir, $class_index, $window_size);

my ($annotated_regions_IT,$total_positives) = load_annotations($annotation_file, $window_size);

open(OUT,">$working_dir/evaluation_MuStARD_scanning_mode.stranded.class_$class_index.csv") or die "$!: $working_dir/evaluation_MuStARD_scanning_mode.stranded.class_$class_index.csv.\n";
print OUT "threshold\tpositives\ttotal\tTP\tFP\tTN\tFN\taccuracy\tprecision\tsensitivity\tspecificity\tspecificityR\n";
for(my $j = 0.5; $j <= 0.9; $j += 0.01 ){

	my $TP = 0;
	my $FP = 0;
	my $TN = 0;
	my $FN = 0;
	foreach my $chrom (keys %$predictions){

		foreach my $strand (keys %{$$predictions{$chrom}}){

			for(my $i = 0; $i < @{$$predictions{$chrom}{$strand}}; $i++){

				my $start = ${${$$predictions{$chrom}{$strand}}[$i]}[1];
				my $stop = ${${$$predictions{$chrom}{$strand}}[$i]}[2];
				my $score = ${${$$predictions{$chrom}{$strand}}[$i]}[3];
# 				die "$start\t$stop\t$score\n";

				my $overlap_flag = 0;

				if( exists $$annotated_regions_IT{$chrom}{$strand} ){

					my $results = $$annotated_regions_IT{$chrom}{$strand}->fetch($start, $stop);

					if( $overlap_fraction == 0 ){

						if( scalar(@$results) >= 1 ){

							$overlap_flag = 1;
						}
					}
					else{

						if( scalar(@$results) >= 1 ){

							$$results[0] =~ /^.+__(.+)__(.+)$/;

							my @tmp = ($start, $stop, $1, $2);
							@tmp = sort { $a <=> $b } @tmp;

							if( ($tmp[2] - $tmp[1]) / $window_size >= $overlap_fraction ){

								$overlap_flag = 1;
							}
						}
					}
				}

				if( $overlap_flag == 0 ){

					if( $score >= $j ){

						$FP++;
					}
					else{

						$TN++;
					}
				}
				else{

					if( $score >= $j ){

						$TP++;
					}
					else{

						$FN++;
					}
				}
			}
		}
	}

	my $accuracy = ($TP + $TN) / ($TP + $TN + $FP + $FN);
	my $precision = $TP / ($TP + $FP);
# 	my $sensitivity = $TP / ($TP + $FN);
	my $sensitivity = $TP / $total_positives;
	my $specificity = $TN / ($TN + $FP);

	print OUT "$j\t$total_positives\t".($TP + $TN + $FP + $FN)."\t$TP\t$FP\t$TN\t$FN\t$accuracy\t$precision\t$sensitivity\t$specificity\t".(1-$specificity)."\n";
}
close OUT;

############################ Subroutines

sub merge_nearby_predictions {

	my ($predictionDir, $classIndex, $windowSize) = @_;

	my %regions;
	my @prediction_files = glob("$predictionDir/predictions.chr*.class_$classIndex.bedGraph.gz");
	foreach my $prediction_file (@prediction_files){

		open(IN,"<:gzip",$prediction_file) or die "$!: $prediction_file.\n";
		while(my $line = <IN>){
			chomp $line;

			if( $line =~ /bedGraph/ ){
				next;
			}

# 			die "$line\n";
			my @temp = split(/\t/,$line);

			my $strand = "+";
			if( $temp[3] < 0 ){
				$strand = "-";
				$temp[3] = abs($temp[3]);
			}

			if( $temp[3] < 0.5 ){
				next;
			}

			my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
			my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

			$temp[1] = $start;
			$temp[2] = $stop;

			push @{$regions{$temp[0]}{$strand}}, \@temp;
		}
		close IN;
	}

	my %clusters;
	foreach my $chrom (keys %regions){

		foreach my $strand (keys %{$regions{$chrom}}){

			@{$regions{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$regions{$chrom}{$strand}};

			my $last_index = "empty";
			for(my $i = 0; $i < @{$regions{$chrom}{$strand}}; $i++){

				if( $last_index eq "empty" ){

					push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
					$last_index = 0;
				}
				else{

					my $previous_stop = ${${$clusters{$chrom}{$strand}}[$last_index]}[2];
					my $previous_score = ${${$clusters{$chrom}{$strand}}[$last_index]}[3];

					my $current_start = ${${$regions{$chrom}{$strand}}[$i]}[1];
					my $current_score = ${${$regions{$chrom}{$strand}}[$i]}[3];

					if( $current_start <= $previous_stop ){

						${${$clusters{$chrom}{$strand}}[$last_index]}[2] = ${${$regions{$chrom}{$strand}}[$i]}[2];

						if( $current_score > $previous_score ){

							${${$clusters{$chrom}{$strand}}[$last_index]}[3] = ${${$regions{$chrom}{$strand}}[$i]}[3];
						}
					}
					else{

						push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
						$last_index++;
					}
				}
			}
		}
	}

	return \%clusters;
}

sub load_annotations {

	my ($inputFile, $windowSize) = @_;

	my (%annotated_loci_IT,$counter);
	open(IN,$inputFile) or die "$!: $inputFile.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

		unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
			next;
		}

		if( $temp[2] - $temp[1] < $windowSize ){

			my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
			my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

			$temp[1] = $start;
			$temp[2] = $stop;
		}

		unless( exists $annotated_loci_IT{$temp[0]}{$temp[5]} ){

			$annotated_loci_IT{$temp[0]}{$temp[5]} = Set::IntervalTree->new;
		}

		my $tmp_name = "$temp[3]__$temp[1]__$temp[2]";

# 		die "$temp[3]\t$temp[1]\t$temp[2]\n";

		$annotated_loci_IT{$temp[0]}{$temp[5]}->insert($tmp_name,$temp[1],$temp[2]);
		$counter++;
	}
	close IN;

	return \%annotated_loci_IT,$counter;
}

sub usage {

	print	"\nUsage: evaluate_MuStARD_scanning_mode.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predDir path/to/bedgraph/dir\t\tDirectory with begraph formatted predictions.\n".
		"\t--outDir path/to/output/dir\t\tOutput directory for saving the results.\n".
		"\t--anotFile path/to/annotation/file\tClass specific annotation file that will be used as ground truth.\n".
		"\t--classIndex INT\t\t\tIndex of class as it appears in the target label used for training.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--winSize INT\t\t\tWindow size that will be used for calculating the overlaps. Must be equal to the corresponding size used for training. Default = 100.\n".
		"\t--overlapFraction (0,1]\t\tFraction of overlap between predictions and annotation. Default = 0.1. Use 0 for at least 1 nt overlap.\n";
	exit;
}
