use warnings;
use strict;

my $user_seed = 0;

my ($help, $input_file);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'seed=i' => \$user_seed,
		'inputFile=s' => \$input_file
	)
or defined $help or (!defined $input_file));

warn "\n\tgenerate_random_predictions with parameters:\n".
	"\t\tInput file - $input_file\n".
	"\t\tSeed - $user_seed\n\n";

my $IN;
if( $input_file =~ /\.gz$/ ){
	open($IN,"<:gzip",$input_file) or die "$!: $input_file.\n";
}
else{
	open($IN,$input_file) or die "$!: $input_file.\n";
}
while(my $line = <$IN>){
	chomp $line;

	my @temp = split(/\t/,$line);

	$temp[4] = rand();

	print join("\t",@temp)."\n";
}
close $IN;

#####################################################################
############################ Subroutines ############################
#####################################################################

sub usage {

	print	"\nUsage: generate_random_predictions.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--inputFile path/to/bed/file\t\tFile with bed formatted loci that will be randomly scored.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\tPrint this help message.\n".
		"\t--seed INT\t\tUser specified seed number. Default = 0.\n";
	exit;
}
