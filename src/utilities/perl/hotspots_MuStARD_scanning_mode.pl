use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $window_size = 100;

my ($help, $prediction_dir, $working_dir, $class_index);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predDir=s' => \$prediction_dir,
		'outDir=s' => \$working_dir,
		'classIndex=i' => \$class_index,
		'winSize=i' => \$window_size
	)
or defined $help or (!defined $prediction_dir or !defined $working_dir or !defined $class_index));

warn "\n\thotspots_MuStARD_scanning_mode in $working_dir with parameters:\n".
	"\t\tPrediction dir - $prediction_dir\n".
	"\t\tClass index - $class_index\n".
	"\t\tWindow size - $window_size\n\n";

system "mkdir $working_dir" unless -d $working_dir;

my ($predictions,$predictions_unstranded) = merge_nearby_predictions($prediction_dir, $class_index, $window_size);

open(OUT,">$working_dir/predictions.merged.stranded.bed") or die "$!: $working_dir/predictions.merged.stranded.bed.\n";
foreach my $chrom ( sort { $a cmp $b } keys(%$predictions)){

	foreach my $strand ( sort { $a cmp $b } keys(%{$$predictions{$chrom}})){

		@{$$predictions{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$$predictions{$chrom}{$strand}};

		for(my $i = 0; $i < @{$$predictions{$chrom}{$strand}}; $i++){

			my $start = ${${$$predictions{$chrom}{$strand}}[$i]}[1];
			my $stop = ${${$$predictions{$chrom}{$strand}}[$i]}[2];
			my $score = ${${$$predictions{$chrom}{$strand}}[$i]}[3];

			my $name = "prediction_$chrom\_$strand\_$i";

			print OUT "$chrom\t$start\t$stop\t$name\t$score\t$strand\n";
		}
	}
}
close OUT;

open(OUT,">$working_dir/predictions.merged.unstranded.bed") or die "$!: $working_dir/predictions.merged.unstranded.bed.\n";
foreach my $chrom ( sort { $a cmp $b } keys(%$predictions_unstranded)){

	@{$$predictions_unstranded{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$$predictions_unstranded{$chrom}};

	for(my $i = 0; $i < @{$$predictions_unstranded{$chrom}}; $i++){

		my $start = ${${$$predictions_unstranded{$chrom}}[$i]}[1];
		my $stop = ${${$$predictions_unstranded{$chrom}}[$i]}[2];
		my $score = ${${$$predictions_unstranded{$chrom}}[$i]}[3];

		my $name = "prediction_$chrom\_$i";

		print OUT "$chrom\t$start\t$stop\t$name\t$score\n";
	}
}
close OUT;

############################ Subroutines
########################################

sub merge_nearby_predictions {

	my ($predictionDir, $classIndex, $windowSize) = @_;

	my (%regions,%regions_unstranded);
	my @prediction_files = glob("$predictionDir/predictions.chr*.class_$classIndex.bedGraph.gz");
	foreach my $prediction_file (@prediction_files){

		open(IN,"<:gzip",$prediction_file) or die "$!: $prediction_file.\n";
		while(my $line = <IN>){
			chomp $line;

			if( $line =~ /bedGraph/ ){
				next;
			}

			my @temp = split(/\t/,$line);

			my $strand = "+";
			if( $temp[3] < 0 ){
				$strand = "-";
				$temp[3] = abs($temp[3]);
			}

			if( $temp[3] < 0.5 ){
				next;
			}

			my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
			my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

			$temp[1] = $start;
			$temp[2] = $stop;

			push @{$regions{$temp[0]}{$strand}}, \@temp;
			push @{$regions_unstranded{$temp[0]}}, \@temp;
		}
		close IN;
	}

	my %clusters;
	foreach my $chrom (keys %regions){

		foreach my $strand (keys %{$regions{$chrom}}){

			@{$regions{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$regions{$chrom}{$strand}};

			my $last_index = "empty";
			for(my $i = 0; $i < @{$regions{$chrom}{$strand}}; $i++){

				if( $last_index eq "empty" ){

					push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
					$last_index = 0;
				}
				else{

					my $previous_stop = ${${$clusters{$chrom}{$strand}}[$last_index]}[2];
					my $previous_score = ${${$clusters{$chrom}{$strand}}[$last_index]}[3];

					my $current_start = ${${$regions{$chrom}{$strand}}[$i]}[1];
					my $current_score = ${${$regions{$chrom}{$strand}}[$i]}[3];

					if( $current_start <= $previous_stop ){

						${${$clusters{$chrom}{$strand}}[$last_index]}[2] = ${${$regions{$chrom}{$strand}}[$i]}[2];

						if( $current_score > $previous_score ){

							${${$clusters{$chrom}{$strand}}[$last_index]}[3] = ${${$regions{$chrom}{$strand}}[$i]}[3];
						}
					}
					else{

						push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
						$last_index++;
					}
				}
			}
		}
	}

	my %clusters_unstranded;
	foreach my $chrom (keys %regions_unstranded){

		@{$regions_unstranded{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$regions_unstranded{$chrom}};

		my $last_index = "empty";
		for(my $i = 0; $i < @{$regions_unstranded{$chrom}}; $i++){

			if( $last_index eq "empty" ){

				push @{$clusters_unstranded{$chrom}}, ${$regions_unstranded{$chrom}}[$i];
				$last_index = 0;
			}
			else{

				my $previous_stop = ${${$clusters_unstranded{$chrom}}[$last_index]}[2];
				my $previous_score = ${${$clusters_unstranded{$chrom}}[$last_index]}[3];

				my $current_start = ${${$regions_unstranded{$chrom}}[$i]}[1];
				my $current_score = ${${$regions_unstranded{$chrom}}[$i]}[3];

				if( $current_start <= $previous_stop ){

					${${$clusters_unstranded{$chrom}}[$last_index]}[2] = ${${$regions_unstranded{$chrom}}[$i]}[2];

					if( $current_score > $previous_score ){

						${${$clusters_unstranded{$chrom}}[$last_index]}[3] = ${${$regions_unstranded{$chrom}}[$i]}[3];
					}
				}
				else{

					push @{$clusters_unstranded{$chrom}}, ${$regions_unstranded{$chrom}}[$i];
					$last_index++;
				}
			}
		}
	}

	return \%clusters,\%clusters_unstranded;
}

sub usage {

	print	"\nUsage: hotspots_MuStARD_scanning_mode.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predDir path/to/bedgraph/dir\t\tDirectory with begraph formatted predictions.\n".
		"\t--outDir path/to/output/dir\t\tOutput directory for saving the results.\n".
		"\t--classIndex INT\t\t\tIndex of class as it appears in the target label used for training.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--winSize INT\t\t\tWindow size that will be used for calculating the overlaps. Must be equal to the corresponding size used for training. Default = 100.\n";
	exit;
}
