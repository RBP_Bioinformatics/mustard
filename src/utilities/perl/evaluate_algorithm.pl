use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $overlap_fraction = 0.1;
my $min_threshold = 0.1;
my $max_threshold = 0.9;
my $stranded = 0;

my ($help, $prediction_file, $annotation_file);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predFile=s' => \$prediction_file,
		'anotFile=s' => \$annotation_file,
		'overlapFraction=f' => \$overlap_fraction,
		'min=f' => \$min_threshold,
		'max=f' => \$max_threshold,
		'stranded=i' => \$stranded,
	)
or defined $help or (!defined $prediction_file or !defined $annotation_file));

warn "\n\tevaluate_algorithm with parameters:\n".
	"\t\tPrediction file - $prediction_file\n".
	"\t\tAnnotation file - $annotation_file\n".
	"\t\tMin threshold - $min_threshold\n".
	"\t\tMax threshold - $max_threshold\n".
	"\t\tStranded - $stranded\n".
	"\t\tOverlap fraction - $overlap_fraction\n\n";

my $predictions = load_predictions($prediction_file, $stranded);

my ($annotated_regions_IT,$total_positives) = load_annotations($annotation_file, $stranded);

print "threshold\tpositives\ttotal\tTP\tFP\tTN\tFN\taccuracy\tprecision\tsensitivity\tspecificity\tspecificityR\n";
for(my $j = $min_threshold; $j <= $max_threshold; $j += 0.02 ){

	my $TP = 0;
	my $FP = 0;
	my $TN = 0;
	my $FN = 0;
	foreach my $chrom (keys %$predictions){

		if( $stranded ){

			foreach my $strand (keys %{$$predictions{$chrom}}){

				for(my $i = 0; $i < @{$$predictions{$chrom}{$strand}}; $i++){

					my $start = ${${$$predictions{$chrom}{$strand}}[$i]}[1];
					my $stop = ${${$$predictions{$chrom}{$strand}}[$i]}[2];
					my $score = ${${$$predictions{$chrom}{$strand}}[$i]}[4];
# 					die "$start\t$stop\t$score\n";

					my $overlap_flag = 0;

					if( exists $$annotated_regions_IT{$chrom}{$strand} ){

						my $results = $$annotated_regions_IT{$chrom}{$strand}->fetch($start, $stop);

						if( $overlap_fraction == 0 ){

							if( scalar(@$results) >= 1 ){

								$overlap_flag = 1;
							}
						}
						else{

							if( scalar(@$results) >= 1 ){

								$$results[0] =~ /^.+__(.+)__(.+)$/;

								my @tmp = ($start, $stop, $1, $2);
								@tmp = sort { $a <=> $b } @tmp;

								if( ($tmp[2] - $tmp[1]) / ($2 - $1) >= $overlap_fraction ){

									$overlap_flag = 1;
								}
							}
						}
					}

					if( $overlap_flag == 0 ){

						if( $score >= $j ){

							$FP++;
						}
						else{

							$TN++;
						}
					}
					else{

						if( $score >= $j ){

							$TP++;
						}
						else{

							$FN++;
						}
					}
				}
			}
		}
		else{

			for(my $i = 0; $i < @{$$predictions{$chrom}}; $i++){

				my $start = ${${$$predictions{$chrom}}[$i]}[1];
				my $stop = ${${$$predictions{$chrom}}[$i]}[2];
				my $score = ${${$$predictions{$chrom}}[$i]}[4];
# 				die "$start\t$stop\t$score\n";

				my $overlap_flag = 0;

				if( exists $$annotated_regions_IT{$chrom} ){

					my $results = $$annotated_regions_IT{$chrom}->fetch($start, $stop);

					if( $overlap_fraction == 0 ){

						if( scalar(@$results) >= 1 ){

							$overlap_flag = 1;
						}
					}
					else{

						if( scalar(@$results) >= 1 ){

							$$results[0] =~ /^.+__(.+)__(.+)$/;

							my @tmp = ($start, $stop, $1, $2);
							@tmp = sort { $a <=> $b } @tmp;

							if( ($tmp[2] - $tmp[1]) / ($2 - $1) >= $overlap_fraction ){

								$overlap_flag = 1;
							}
						}
					}
				}

				if( $overlap_flag == 0 ){

					if( $score >= $j ){

						$FP++;
					}
					else{

						$TN++;
					}
				}
				else{

					if( $score >= $j ){

						$TP++;
					}
					else{

						$FN++;
					}
				}
			}
		}
	}

	my $accuracy = ($TP + $TN) / ($TP + $TN + $FP + $FN);
	my $precision = $TP / ($TP + $FP);

	my $sensitivity = $TP / ($TP + $FN);
	if( $min_threshold == 0.5 ){
		$sensitivity = $TP / $total_positives;
	}

	my $specificity = $TN / ($TN + $FP);

	print "$j\t$total_positives\t".($TP + $TN + $FP + $FN)."\t$TP\t$FP\t$TN\t$FN\t$accuracy\t$precision\t$sensitivity\t$specificity\t".(1-$specificity)."\n";
}

#####################################################################
############################ Subroutines ############################
#####################################################################

sub load_predictions {

	my ($predictionFile, $strand_mode) = @_;

	my %regions;
	open(IN,$predictionFile) or die "$!: $predictionFile.\n";
	while(my $line = <IN>){
		chomp $line;

# 		die "$line\n";
		my @temp = split(/\t/,$line);

		if( $strand_mode ){

			push @{$regions{$temp[0]}{$temp[5]}}, \@temp;
		}
		else{

			push @{$regions{$temp[0]}}, \@temp;
		}
	}
	close IN;

	return \%regions;
}

sub load_annotations {

	my ($inputFile, $strand_mode) = @_;

	my (%annotated_loci_IT,$counter);
	open(IN,$inputFile) or die "$!: $inputFile.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

		unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
			next;
		}

		if( $strand_mode ){

			unless( exists $annotated_loci_IT{$temp[0]}{$temp[5]} ){

				$annotated_loci_IT{$temp[0]}{$temp[5]} = Set::IntervalTree->new;
			}
		}
		else{

			unless( exists $annotated_loci_IT{$temp[0]} ){

				$annotated_loci_IT{$temp[0]} = Set::IntervalTree->new;
			}
		}

		my $tmp_name = "$temp[3]__$temp[1]__$temp[2]";

# 		die "$temp[3]\t$temp[1]\t$temp[2]\n";

		if( $strand_mode ){

			$annotated_loci_IT{$temp[0]}{$temp[5]}->insert($tmp_name,$temp[1],$temp[2]);
		}
		else{

			$annotated_loci_IT{$temp[0]}->insert($tmp_name,$temp[1],$temp[2]);
		}

		$counter++;
	}
	close IN;

	return \%annotated_loci_IT,$counter;
}

sub usage {

	print	"\nUsage: evaluate_algorithm.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predFile path/to/bed/file\t\tFile with bed formatted predictions.\n".
		"\t--anotFile path/to/annotation/file\tClass specific bed formatted annotation file that will be used as ground truth.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--min (0,1)\t\t\tMinimum score threshold. Default = 0.1.\n".
		"\t--max (0,1)\t\t\tMaximum score threshold. Default = 0.9.\n".
		"\t--stranded [0|1]\t\t\tEvaluate based on strand. Default = 0, no strand usage.\n".
		"\t--overlapFraction (0,1]\t\tFraction of overlap between predictions and annotation. Default = 0.1. Use 0 for at least 1 nt overlap.\n";
	exit;
}
