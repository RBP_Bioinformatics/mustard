use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $window_size = 100;

my ($help, $prediction_file, $out_prefix);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predFile=s' => \$prediction_file,
		'outPrefix=s' => \$out_prefix,
		'winSize=i' => \$window_size
	)
or defined $help or (!defined $prediction_file or !defined $out_prefix));

warn "\n\tcall_hotspots.pl with parameters:\n".
	"\t\tPrediction file - $prediction_file\n".
	"\t\tOutput prefix - $out_prefix\n".
	"\t\tWindow size - $window_size\n\n";

my ($predictions,$predictions_unstranded) = merge_nearby_predictions($prediction_file, $window_size);

open(OUT,">$out_prefix.hotspots.stranded.bed") or die "$!: $out_prefix.hotspots.stranded.bed.\n";
foreach my $chrom ( sort { $a cmp $b } keys(%$predictions)){

	foreach my $strand ( sort { $a cmp $b } keys(%{$$predictions{$chrom}})){

		@{$$predictions{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$$predictions{$chrom}{$strand}};

		for(my $i = 0; $i < @{$$predictions{$chrom}{$strand}}; $i++){

			my $start = ${${$$predictions{$chrom}{$strand}}[$i]}[1];
			my $stop = ${${$$predictions{$chrom}{$strand}}[$i]}[2];
			my $score = ${${$$predictions{$chrom}{$strand}}[$i]}[3];

			my $name = "prediction_$chrom\_$strand\_$i";

			print OUT "$chrom\t$start\t$stop\t$name\t$score\t$strand\n";
		}
	}
}
close OUT;

open(OUT,">$out_prefix.hotspots.unstranded.bed") or die "$!: $out_prefix.hotspots.unstranded.bed.\n";
foreach my $chrom ( sort { $a cmp $b } keys(%$predictions_unstranded)){

	@{$$predictions_unstranded{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$$predictions_unstranded{$chrom}};

	for(my $i = 0; $i < @{$$predictions_unstranded{$chrom}}; $i++){

		my $start = ${${$$predictions_unstranded{$chrom}}[$i]}[1];
		my $stop = ${${$$predictions_unstranded{$chrom}}[$i]}[2];
		my $score = ${${$$predictions_unstranded{$chrom}}[$i]}[3];

		my $name = "prediction_$chrom\_$i";

		print OUT "$chrom\t$start\t$stop\t$name\t$score\n";
	}
}
close OUT;

###################################################################
############################ Subroutines ##########################
###################################################################

sub merge_nearby_predictions {

	my ($predictionFile, $windowSize) = @_;

	my (%regions,%regions_unstranded);
	open(IN,"<:gzip",$predictionFile) or die "$!: $predictionFile.\n";
	while(my $line = <IN>){
		chomp $line;

		if( $line =~ /bedGraph/ ){
			next;
		}

		my @temp = split(/\t/,$line);

		my $strand = "+";
		if( $temp[3] < 0 ){
			$strand = "-";
			$temp[3] = abs($temp[3]);
		}

		if( $temp[3] < 0.5 ){
			next;
		}

		my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
		my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

		$temp[1] = $start;
		$temp[2] = $stop;

		my @tmp_stranded = @temp;
		my @tmp_unstranded = @temp;

		push @{$regions{$temp[0]}{$strand}}, \@tmp_stranded;
		push @{$regions_unstranded{$temp[0]}}, \@tmp_unstranded;
	}
	close IN;

	my %clusters;
	foreach my $chrom (keys %regions){

		foreach my $strand (keys %{$regions{$chrom}}){

			@{$regions{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$regions{$chrom}{$strand}};

			my $last_index = "empty";
			for(my $i = 0; $i < @{$regions{$chrom}{$strand}}; $i++){

				if( $last_index eq "empty" ){

					push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
					$last_index = 0;
				}
				else{

					my $previous_stop = ${${$clusters{$chrom}{$strand}}[$last_index]}[2];
					my $previous_score = ${${$clusters{$chrom}{$strand}}[$last_index]}[3];

					my $current_start = ${${$regions{$chrom}{$strand}}[$i]}[1];
					my $current_score = ${${$regions{$chrom}{$strand}}[$i]}[3];

					if( $current_start <= $previous_stop ){

						${${$clusters{$chrom}{$strand}}[$last_index]}[2] = ${${$regions{$chrom}{$strand}}[$i]}[2];

						if( $current_score > $previous_score ){

							${${$clusters{$chrom}{$strand}}[$last_index]}[3] = ${${$regions{$chrom}{$strand}}[$i]}[3];
						}
					}
					else{

						push @{$clusters{$chrom}{$strand}}, ${$regions{$chrom}{$strand}}[$i];
						$last_index++;
					}
				}
			}
		}
	}

	my %clusters_unstranded;
	foreach my $chrom (keys %regions_unstranded){

		@{$regions_unstranded{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$regions_unstranded{$chrom}};

		my $last_index = "empty";
		for(my $i = 0; $i < @{$regions_unstranded{$chrom}}; $i++){

			if( $last_index eq "empty" ){

				push @{$clusters_unstranded{$chrom}}, ${$regions_unstranded{$chrom}}[$i];
				$last_index = 0;
			}
			else{

				my $previous_stop = ${${$clusters_unstranded{$chrom}}[$last_index]}[2];
				my $previous_score = ${${$clusters_unstranded{$chrom}}[$last_index]}[3];

				my $current_start = ${${$regions_unstranded{$chrom}}[$i]}[1];
				my $current_score = ${${$regions_unstranded{$chrom}}[$i]}[3];

				if( $current_start <= $previous_stop ){

					${${$clusters_unstranded{$chrom}}[$last_index]}[2] = ${${$regions_unstranded{$chrom}}[$i]}[2];

					if( $current_score > $previous_score ){

						${${$clusters_unstranded{$chrom}}[$last_index]}[3] = ${${$regions_unstranded{$chrom}}[$i]}[3];
					}
				}
				else{

					push @{$clusters_unstranded{$chrom}}, ${$regions_unstranded{$chrom}}[$i];
					$last_index++;
				}
			}
		}
	}

	return \%clusters,\%clusters_unstranded;
}

sub usage {

	print	"\nUsage: call_hotspots.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predFile path/to/bedgraph/file\t\tDirectory with begraph formatted predictions.\n".
		"\t--outPrefix output/prefix\t\t\tOutput prefix for saving the results.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--winSize INT\t\t\tWindow size that will be used for calculating the overlaps. Must be equal to the corresponding size used for training. Default = 100.\n";
	exit;
}
