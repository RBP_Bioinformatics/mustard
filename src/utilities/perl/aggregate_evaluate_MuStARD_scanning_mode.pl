use warnings;
use strict;

unless( @ARGV == 2 ){
	die "Wrong number of arguments.\nUsage: perl $0 <result_dir> <output_dir>\n";
}

my ($result_dir, $output_dir) = @ARGV;

system "mkdir $output_dir" unless -d $output_dir;

my %data;
my @subdirectories = grep {-d} glob("$result_dir/permutation_*");
foreach my $subdirectory (@subdirectories){

	unless( -e "$subdirectory/evaluation/evaluation_MuStARD_scanning_mode.stranded.class_0.csv" ){
		next;
	}

	my @header;
	open(IN,"$subdirectory/evaluation/evaluation_MuStARD_scanning_mode.stranded.class_0.csv") or die "$!: $subdirectory/evaluation/evaluation_MuStARD_scanning_mode.stranded.class_0.csv.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

# 		threshold	positives	total	TP	FP	TN	FN	accuracy	precision	sensitivity	specificity	specificityR
# 		0.5	53	287	48	239	0	0	0.167247386759582	0.167247386759582	0.905660377358491	0	1
# 		0.89	53	287	31	15	224	17	0.888501742160279	0.673913043478261	0.584905660377358	0.937238493723849	0.0627615062761506

		if( $temp[0] =~ /^threshold$/ ){

			@header = @temp;
		}
		elsif( $temp[0] =~ /^0\.5$/ or $temp[0] =~ /^0\.89$/ ){

			for(my $i = 2; $i < @temp; $i++){

				my @tmp = ($subdirectory, $temp[$i]);

				push @{$data{$header[$i]}{$temp[0]}}, \@tmp;
			}
		}
		else{
			next;
		}
	}
	close IN;
}

foreach my $metric (keys %data){

	if( $metric eq "TN" or $metric eq "FN" or $metric eq "accuracy" or $metric eq "specificity" or $metric eq "specificityR" ){
		next;
	}

	open(OUT,">$output_dir/$metric.csv") or die "$!: $output_dir/$metric.csv.\n";
	print OUT "Threshold\tFile\tValue\n";
	foreach my $threshold (keys %{$data{$metric}}){

		@{$data{$metric}{$threshold}} = sort { $$a[1] <=> $$b[1] } @{$data{$metric}{$threshold}};

		for(my $i = 0; $i < @{$data{$metric}{$threshold}}; $i++){

			print OUT "$threshold\t".join("\t", @{${$data{$metric}{$threshold}}[$i]})."\n";
		}
	}
	close OUT;

	system "Rscript /home/georgeg/workspace/dev/MuStARD/src/utilities/R/aggregate_evaluate_MuStARD.R $output_dir/$metric";
}
