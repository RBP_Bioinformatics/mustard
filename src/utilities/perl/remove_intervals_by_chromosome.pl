use warnings;
use strict;

my ($help, $species, $input_file);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'species=s' => \$species,
		'inputFile=s' => \$input_file
	)
or defined $help or (!defined $species or !defined $input_file));

warn "\n\tremove_intervals_by_chromosome with parameters:\n".
	"\t\tInput file - $input_file\n".
	"\t\tSpecies - $species\n\n";

my $chr_valid = get_valid_chromosomes($species);

open(IN,$input_file) or die "$!: $input_file.\n";
while(my $line = <IN>){
	chomp $line;

	my @temp = split(/\t/,$line);

	unless( exists $$chr_valid{$temp[0]} ){
		next;
	}

	print "$line\n";
}
close IN;

###################################################################################################
########################################### Subroutines ###########################################
###################################################################################################

sub usage {

	print	"\nUsage: remove_intervals_by_chromosome.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--inputFile path/to/bed/file\t\tPath of bed formatted file that will be filtered for unwanted chromosomes.\n".
		"\t--species [hsa|mmu|cel|...]\t\tSpecies identifier that will be used to retrieve valid chromosomes.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\tPrint this help message.\n";
	exit;
}

sub get_valid_chromosomes {

	my ($tmp_species) = @_;

	my %valid_chromosomes = (

			'hsa' => {
				'chr1' => 1,
				'chr2' => 1,
				'chr3' => 1,
				'chr4' => 1,
				'chr5' => 1,
				'chr6' => 1,
				'chr7' => 1,
				'chr8' => 1,
				'chr9' => 1,
				'chr10' => 1,
				'chr11' => 1,
				'chr12' => 1,
				'chr13' => 1,
				'chr14' => 1,
				'chr15' => 1,
				'chr16' => 1,
				'chr17' => 1,
				'chr18' => 1,
				'chr19' => 1,
				'chr20' => 1,
				'chr21' => 1,
				'chr22' => 1,
				'chrX' => 1
			},
			'mmu' => {
				'chr1' => 1,
				'chr2' => 1,
				'chr3' => 1,
				'chr4' => 1,
				'chr5' => 1,
				'chr6' => 1,
				'chr7' => 1,
				'chr8' => 1,
				'chr9' => 1,
				'chr10' => 1,
				'chr11' => 1,
				'chr12' => 1,
				'chr13' => 1,
				'chr14' => 1,
				'chr15' => 1,
				'chr16' => 1,
				'chr17' => 1,
				'chr18' => 1,
				'chr19' => 1,
				'chrX' => 1
			},
			'cel' => {
				'chrI' => 1,
				'chrII' => 1,
				'chrIII' => 1,
				'chrIV' => 1,
				'chrV' => 1,
				'chrX' => 1
			}
		);

	return \%{$valid_chromosomes{$tmp_species}};
}
