use warnings;
use strict;

unless( @ARGV == 2 ){
	die "Wrong number of arguments.\nUsage: perl $0 <performance_dir> <type>\n";
}

my ($performance_dir, $type) = @ARGV;

opendir my $dh, "$performance_dir/$type" or die "$0: opendir: $!";

my (%results, @header_file, @header_case);
my @dirs = grep {-d "$performance_dir/$type/$_" && ! /^\.{1,2}$/} readdir($dh);
foreach my $dir (@dirs){

	unless( -e "$performance_dir/$type/$dir/CNNonRaw.acc.png" ){
		next;
	}

	my @header_dir = split(/_/, $dir);
# 	warn join("\t",@header_dir)."\n";
	for(my $i = 0; $i < @header_dir; $i++){

		if( $header_dir[$i] =~ /\./ ){
			$header_dir[$i] =~ /^(.+?)(\d+\.\d+)$/;
			$header_case[$i] = $1;
			$header_dir[$i] = $2;
		}
		else{
			$header_dir[$i] =~ /^([a-zA-Z].+?)(\d+)$/;
			$header_case[$i] = $1;
			$header_dir[$i] = $2;
		}
	}
# 	warn join("\t",@header_dir)."\n";
# 	die join("\t",@header_case)."\n";
	my $header_dir_str = join("\t",@header_dir);

	$results{$header_dir_str}{"epoch"} = 0;
	$results{$header_dir_str}{"acc"} = 0;
	$results{$header_dir_str}{"loss"} = 0;
	$results{$header_dir_str}{"val_acc"} = 0;
	$results{$header_dir_str}{"val_loss"} = 1000000000;
	open(IN,"$performance_dir/$type/$dir/CNNonRaw.log.csv") or die "$!: $performance_dir/$type/$dir/CNNonRaw.log.csv\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

		if( $temp[0] eq "epoch" ){
			@header_file = @temp;
		}
		else{

			$temp[0]++;
			if( $temp[4] < $results{$header_dir_str}{"val_loss"} ){
				$results{$header_dir_str}{"val_loss"} = $temp[4];
				$results{$header_dir_str}{"epoch"} = $temp[0];
				$results{$header_dir_str}{"acc"} = $temp[1];
				$results{$header_dir_str}{"loss"} = $temp[2];
				$results{$header_dir_str}{"val_acc"} = $temp[3];
			}
		}
	}
	close IN;

# 	warn "$dir\n";
}

open(OUT,">$performance_dir/$type.aggregated.performance.log.csv") or die "$!: $performance_dir/$type.aggregated.performance.log.csv\n";
print OUT join("\t",@header_case)."\t".join("\t",@header_file)."\n";
foreach my $header_dir_str (sort { $a cmp $b } keys(%results)){

	print OUT "$header_dir_str\t".$results{$header_dir_str}{"epoch"}."\t".$results{$header_dir_str}{"acc"}."\t".$results{$header_dir_str}{"loss"}."\t".$results{$header_dir_str}{"val_acc"}."\t".$results{$header_dir_str}{"val_loss"}."\n";
}
close OUT;