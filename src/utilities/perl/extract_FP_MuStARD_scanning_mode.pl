use warnings;
use strict;

use Set::IntervalTree;

unless( @ARGV == 5 ){
	die "Wrong number of arguments.\nUsage: perl $0 <annotation_file> <window_size> <unique_id> <result_dir> <output_dir>\n";
}

my ($annotation_file, $window_size, $unique_id, $result_dir, $output_dir) = @ARGV;

system "mkdir $output_dir" unless -d $output_dir;

open(OUT,">$output_dir/all.predictions.bed") or die "$!: $output_dir/all.predictions.bed.\n";

warn "\n\tLocating best predicion per hotspot.\n";
my (%bedgraphs, @dir_names);
my @subdirectories = grep {-d} glob("$result_dir/permutation_*");
foreach my $subdirectory (@subdirectories){

	$subdirectory =~ /^$result_dir\/(permutation_\d+)$/;
	my $dir_name = $1;

	warn "\t\tOn $dir_name.\n";

	unless( -e "$subdirectory/evaluation/predictions.merged.stranded.bed" ){
		next;
	}

	push @dir_names, $dir_name;

	warn "\t\t\tLoading prediction from bedGraph.\n";
	my $bedgraph = load_regions("$subdirectory/bedGraph_tracks/all.predictions.class_0.bedGraph.gz", $window_size);

	warn "\t\t\tFinding overlap with hotspots.\n";
	open(IN,"$subdirectory/evaluation/predictions.merged.stranded.bed") or die "$!: $subdirectory/evaluation/predictions.merged.stranded.bed.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

		my $tmp_results = $$bedgraph{$temp[0]}{$temp[5]}->fetch($temp[1], $temp[2]);

		my @tmp_results_splitted;
		if( scalar(@$tmp_results) < 1 ){
			die "ERROR!\n";
		}
		else{

			foreach my $ele (@$tmp_results){

				my @tmp = split(/__/, $ele);
				push @tmp_results_splitted, \@tmp;
			}
		}

		@tmp_results_splitted = sort { $$b[0] <=> $$a[0] } @tmp_results_splitted;

		my $tmp_results_splitted_score = ${$tmp_results_splitted[0]}[0];
		my $tmp_results_splitted_start = ${$tmp_results_splitted[0]}[1];
		my $tmp_results_splitted_stop = ${$tmp_results_splitted[0]}[2];
		my $tmp_results_splitted_strand = ${$tmp_results_splitted[0]}[3];
		my $tmp_results_splitted_joined = "$tmp_results_splitted_score\__$tmp_results_splitted_start\__$tmp_results_splitted_stop\__$tmp_results_splitted_strand";

		unless( exists $bedgraphs{$dir_name}{$temp[0]} ){
			$bedgraphs{$dir_name}{$temp[0]} = Set::IntervalTree->new();
		}

		$bedgraphs{$dir_name}{$temp[0]}->insert($tmp_results_splitted_joined, $tmp_results_splitted_start, $tmp_results_splitted_stop);

		print OUT "$line\n";
	}
	close IN;
}
close OUT;

warn "\n\tMerging hotspots across permutations.\n";
system "bedtools sort -i $output_dir/all.predictions.bed | bedtools merge > $output_dir/all.predictions.merged.bed";

warn "\tLoading annotation.\n";
my $annotated_regions_IT = load_regions($annotation_file, -1);

my ($tp_counter, $fp_counter);
warn "\tFiltering TPs and FPs.\n";
open(FP,">$output_dir/all.predictions.FP.bed") or die "$!: $output_dir/all.predictions.FP.bed.\n";
open(TP,">$output_dir/all.predictions.TP.bed") or die "$!: $output_dir/all.predictions.TP.bed.\n";
open(IN,"$output_dir/all.predictions.merged.bed") or die "$!: $output_dir/all.predictions.merged.bed.\n";
while(my $line = <IN>){
	chomp $line;

	my @temp = split(/\t/,$line);

	my ($counter, @best_results);
	foreach my $dir_name (@dir_names){

		my $tmp_results = $bedgraphs{$dir_name}{$temp[0]}->fetch($temp[1], $temp[2]);

		if( scalar(@$tmp_results) > 0 ){

			$counter++;

			foreach my $ele (@$tmp_results){

				my @tmp = split(/__/, $ele);

				if( scalar(@best_results) < 1 ){

					@best_results = @tmp;
				}
				else{

					if( $tmp[0] > $best_results[0] ){

						@best_results = @tmp;
					}
				}
			}
		}
	}

	if( $counter >= int(scalar(@dir_names) / 2) ){

		my $tmp_results = $$annotated_regions_IT{$temp[0]}->fetch($best_results[1], $best_results[2]);

		$best_results[1] += int(rand(20));
		$best_results[2] -= int(rand(20));

		if( scalar(@$tmp_results) < 1 ){

			$fp_counter++;
			print FP "$temp[0]\t$best_results[1]\t$best_results[2]\tFP_$fp_counter|$unique_id\t$best_results[0]\t$best_results[3]\n";
		}
		else{

			$tp_counter++;
			print TP "$temp[0]\t$best_results[1]\t$best_results[2]\tTP_$tp_counter|$unique_id\t$best_results[0]\t$best_results[3]\n";
		}
	}
}
close IN;
close FP;
close TP;

############## Subroutines
##########################

sub load_regions {

	my ($inputFile, $windowSize) = @_;

	my (%annotated_loci_IT,$IN);
	if( $inputFile =~ /\.gz$/ ){
		open($IN,"<:gzip",$inputFile) or die "$!: $inputFile.\n";
	}
	else{
		open($IN,$inputFile) or die "$!: $inputFile.\n";
	}
	while(my $line = <$IN>){
		chomp $line;

		if( $line =~ /^track/ ){
			next;
		}

		my @temp = split(/\t/,$line);

		unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
			next;
		}

		unless( $windowSize == -1 ){

			if( $temp[2] - $temp[1] < $windowSize ){

				my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
				my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

				$temp[1] = $start;
				$temp[2] = $stop;
			}
		}

		if( scalar(@temp) < 6 ){

			my $strand = "+";
			if( $temp[3] < 0 ){

				$strand = "-";
			}

			unless( exists $annotated_loci_IT{$temp[0]}{$strand} ){

				$annotated_loci_IT{$temp[0]}{$strand} = Set::IntervalTree->new;
			}

			my $tmp_name = "$temp[3]__$temp[1]__$temp[2]__$strand";

# 			die "$temp[3]\t$temp[1]\t$temp[2]\n";

			$annotated_loci_IT{$temp[0]}{$strand}->insert($tmp_name,$temp[1],$temp[2]);
		}
		else{

			unless( exists $annotated_loci_IT{$temp[0]} ){

				$annotated_loci_IT{$temp[0]} = Set::IntervalTree->new;
			}

			my $tmp_name = "$temp[3]__$temp[1]__$temp[2]";

# 			die "$temp[3]\t$temp[1]\t$temp[2]\n";

			$annotated_loci_IT{$temp[0]}->insert($tmp_name,$temp[1],$temp[2]);
		}
	}
	close $IN;

	return \%annotated_loci_IT;
}
