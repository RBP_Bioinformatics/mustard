use warnings;
use strict;

unless( @ARGV == 1 ){
	die "Wrong number of arguments.\nUsage: perl $0 <ini_file>\n";
}

my ($ini_file) = @ARGV;

# Load initialization file
my $parameters = load_ini_file($ini_file);

my $output_dir = $$parameters{"outputDir"};
my $permutation_string = $$parameters{"permString"};
my $pos_sub_sample_string = $$parameters{"posSubSampleString"};
my $neg_over_pos_string = $$parameters{"negOverPosString"};
my $genome_table = $$parameters{"genomeTable"};
my $test_locus = $$parameters{"testLocus"};
my $annotation_file = $$parameters{"annotationFile"};

my $class = $$parameters{"class"};
my $max_size = $$parameters{"maxSize"};
my $reinf_num = $$parameters{"reinfNum"};
my $ext_flag = $$parameters{"extFlag"};
my $shuf_class_flag = $$parameters{"shufClassFlag"};
my $input_mode = $$parameters{"inputMode"};
my $excl_test = $$parameters{"exclTest"};
my $excl_valid = $$parameters{"exclValid"};
my $genome = $$parameters{"genome"};
my $cons_dir = $$parameters{"consDir"};
my $dir = $$parameters{"dir"};
my $pred_dir = $$parameters{"predDir"};

my $positive_file = $$parameters{"positive"};
my @negative_list = split(/,/, $$parameters{"negative"});

my $log_file = $$parameters{"logFile"};

# Start log file
open(LOGOUT,">$log_file") or die "$!: $log_file\n";
print LOGOUT "Initialization file used: $ini_file\n";

# Ensure that all directories exist
system "mkdir $dir" unless -d $dir;
print LOGOUT "Training directory: $dir\n";

system "mkdir $output_dir" unless -d $output_dir;
print LOGOUT "Data directory: $output_dir\n";

system "mkdir $pred_dir" unless -d $pred_dir;
print LOGOUT "Prediction directory: $pred_dir\n";

# Rename the input mode and use it for naming various directories
my $tmp_input_mode = $input_mode;
$tmp_input_mode =~ s/,/_/g;

# Parse permutation parameters
my @permutation_list = split(/,/,$permutation_string);
my @pos_sub_sample_list = split(/,/,$pos_sub_sample_string);
my @neg_over_pos_list = split(/,/,$neg_over_pos_string);

# Create the intermediate directory that will host the one-pass scanning files
system "mkdir $pred_dir/intermediate_files" unless -d "$pred_dir/intermediate_files";

# Initiate the rounds of negatives selection
for(my $x = 1; $x <= @permutation_list; $x++ ){

	# Prepare iteration directories
	system "mkdir $dir/iteration_$x" unless -d "$dir/iteration_$x";
	print LOGOUT "\n#### Iteration $x\n";

	system "mkdir $pred_dir/intermediate_files/iteration_$x" unless -d "$pred_dir/intermediate_files/iteration_$x";

	system "mkdir $output_dir/iteration_$x" unless -d "$output_dir/iteration_$x";
	system "mkdir $output_dir/iteration_$x/after.FP.selection" unless -d "$output_dir/iteration_$x/after.FP.selection";
	system "mkdir $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode" unless -d "$output_dir/iteration_$x/after.FP.selection/$tmp_input_mode";

	system "mkdir $pred_dir/iteration_$x" unless -d "$pred_dir/iteration_$x";

	my $pos_sub_sample = $pos_sub_sample_list[$x - 1];
	my $neg_over_pos = $neg_over_pos_list[$x - 1];

	my $tmp_dir_predict = "$pred_dir/iteration_$x/all.permutation.results";
	system "mkdir $tmp_dir_predict" unless -d $tmp_dir_predict;

	# Initiate the training of permutations models
	for(my $i = 1; $i <= $permutation_list[$x - 1]; $i++){

		my $working_dir = $output_dir."/iteration_$x"."/permutation_$i";
		system "mkdir $working_dir" unless -d $working_dir;

		system "mkdir $pred_dir/intermediate_files/iteration_$x/permutation_$i" unless -d "$pred_dir/intermediate_files/iteration_$x/permutation_$i";

		my $tmp_seed = 100*$x + $i;

		unless( -e "$working_dir/all.positives.bed" ){

			if( $pos_sub_sample =~ /^\d{1,}$/ ){

				system "bedtools sample -n $pos_sub_sample -seed $tmp_seed -i $positive_file > $working_dir/all.positives.bed";
			}
			else{

				open(OUT,">$working_dir/all.positives.bed") or die "$!: $working_dir/all.positives.bed\n";
				open(IN,$positive_file) or die "$!: $positive_file\n";
				while(my $line = <IN>){
					chomp $line;

					print OUT "$line\n";
				}
				close IN;
				close OUT;
			}

			my $tmp_command = `wc -l $working_dir/all.positives.bed`;
			print LOGOUT "\t\tPositives: $tmp_command\n";
		}

		unless( -e "$working_dir/all.negatives.bed" ){

			print LOGOUT "\n\tPermutation $i\n\n";

			if( $x == 1 ){

				# If the current iteration is the first one, sample negatives from the files specified in the ini file
				# Iterate through each file and sample 1:1 based on the positive set
				# Open each shuffled file and change its name for the purposes of MuStARD

				my (@tmp_negative, $counter);
				for(my $j = 0; $j < @negative_list; $j++){

					my $tmp_negative_file = $negative_list[$j];

					system "bedtools shuffle -seed $tmp_seed -chrom -noOverlapping -incl $tmp_negative_file -i $working_dir/all.positives.bed -g $genome_table > $working_dir/negative_$j.bed";
					my $tmp_command = `wc -l $working_dir/negative_$j.bed`;
					print LOGOUT "\t\t$tmp_command\n";

					open(OUT,">$working_dir/negative_$j.fixedName.bed") or die "$!: $working_dir/negative_$j.fixedName.bed.\n";
					open(IN,"$working_dir/negative_$j.bed") or die "$!: $working_dir/negative_$j.bed.\n";
					while(my $line = <IN>){
						chomp $line;

						$counter++;

						my @temp = split(/\t/,$line);

						$temp[3] = "shuffle_$counter";

						print OUT join("\t", @temp)."\n";
					}
					close OUT;
					close IN;

					push @tmp_negative, "$working_dir/negative_$j.fixedName.bed";
				}

				my $cat_command = "cat ".join(" ", @tmp_negative)." > $working_dir/all.negatives.bed";
				system "$cat_command";
			}
			else{

				# If the current iteration is not the first one, try to sample negatives from the FP pool
				# Count the number of negatives and calculate the total negative number based on the negOverPos parameter in ini
				# If the total number of negatives is higher than the number of pooled negatives (FP) then include every pooled negative
				# Else do a sampling process

				my $tmp_file_obligatory = "$output_dir/iteration_".($x - 1)."/after.FP.selection/$tmp_input_mode/all.negatives.noFPoverlap.bed";
				my $tmp_command_obl = `wc -l $tmp_file_obligatory`;
				print LOGOUT "\t\tPrevious Negatives: $tmp_command_obl\n";

				my $tmp_file = "$output_dir/iteration_".($x - 1)."/after.FP.selection/$tmp_input_mode/all.predictions.FP.noPermutationOverlap.bed";
				my $tmp_command = `wc -l $tmp_file`;
				print LOGOUT "\t\tCurrent FPs: $tmp_command\n";

				$tmp_command =~ /^(\d{1,})\s+$tmp_file$/;
				my $tmp_all_negatives = $1;

				my $neg_sub_sample = "all";
				if( $neg_over_pos =~ /^\d{1,}$/ ){

					my $tmp_command_pos = `wc -l $positive_file`;
					$tmp_command_pos =~ /^(\d{1,})\s+$positive_file$/;
					my $tmp_all_positives = $1;

					if( $pos_sub_sample =~ /^\d{1,}$/ ){

						$neg_sub_sample = $pos_sub_sample * $neg_over_pos;
					}
					else{

						$neg_sub_sample = $tmp_all_positives * $neg_over_pos;
					}
				}

				if( $neg_sub_sample !~ /^\d{1,}$/ or $neg_sub_sample > $tmp_all_negatives ){

					system "cat $tmp_file_obligatory $tmp_file > $working_dir/all.negatives.bed";
				}
				else{

					system "bedtools sample -n $neg_sub_sample -seed $tmp_seed -i $tmp_file > $working_dir/all.predictions.FP.noPermutationOverlap.subsampled.bed";
					system "cat $tmp_file_obligatory $working_dir/all.predictions.FP.noPermutationOverlap.subsampled.bed > $working_dir/all.negatives.bed";
				}

				my $tmp_command_new_neg = `wc -l $working_dir/all.negatives.bed`;
				print LOGOUT "\t\tNew Negatives: $tmp_command_new_neg\n";
			}
		}

		my $tmp_dir_train = "$dir/iteration_$x/permutation_$i";
		system "mkdir $tmp_dir_train" unless -d $tmp_dir_train;

		my $tmp_input = "$working_dir/all.positives.bed,$working_dir/all.negatives.bed";

		system "bedtools intersect -u -a $test_locus -b $working_dir/all.positives.bed > $working_dir/test_locus.bed";
		my $tmp_test_locus = "$working_dir/test_locus.bed";

		my $mustard_command_train = "./MuStARD.pl train --modelType CNN --class $class --maxSize $max_size --reinfNum $reinf_num --extFlag $ext_flag --shufClassFlag $shuf_class_flag --inputMode $input_mode --exclTest $excl_test --exclValid $excl_valid --list $tmp_input --genome $genome --consDir $cons_dir --dir $tmp_dir_train";

		system "$mustard_command_train" unless -e "$tmp_dir_train/Models/$tmp_input_mode/batch256_dropout0.2_lr0.0001_filters40/CNNonRaw.log.csv";

		my $tmp_intermediate_directory = "$pred_dir/intermediate_files/iteration_$x/permutation_$i";

		my $mustard_command_predict = "./MuStARD.pl predict --winSize $max_size --staticPredFlag 0 --step 10 --intermDir $tmp_intermediate_directory --modelDirName permutation_$i --inputMode $input_mode --chromList all --targetIntervals $tmp_test_locus --genome $genome --consDir $cons_dir --dir $tmp_dir_predict --model $tmp_dir_train/Models/$tmp_input_mode/batch256_dropout0.2_lr0.0001_filters40/CNNonRaw.hdf5 --modelType CNN --classNum 2";

		system "$mustard_command_predict" unless -d "$tmp_dir_predict/predict/scan/permutation_$i/intermediate_files";
# 		system "$mustard_command_predict";

		my $evaluation_command = "perl src/utilities/perl/evaluate_MuStARD_scanning_mode.pl --winSize $max_size --overlapFraction 0 --classIndex 0 --anotFile $annotation_file --predDir $tmp_dir_predict/predict/scan/permutation_$i/bedGraph_tracks --outDir $tmp_dir_predict/predict/scan/permutation_$i/evaluation && Rscript src/utilities/R/evaluate_MuStARD.R $tmp_dir_predict/predict/scan/permutation_$i/evaluation/evaluation_MuStARD_scanning_mode.stranded.class_0 && perl src/utilities/perl/hotspots_MuStARD_scanning_mode.pl --winSize $max_size --classIndex 0 --predDir $tmp_dir_predict/predict/scan/permutation_$i/bedGraph_tracks --outDir $tmp_dir_predict/predict/scan/permutation_$i/evaluation";

		system "$evaluation_command" unless -d "$tmp_dir_predict/predict/scan/permutation_$i/evaluation";

		my $hotspot_command = "perl src/utilities/perl/hotspots_MuStARD_scanning_mode.pl --winSize 100 --classIndex 0 --predDir $tmp_dir_predict/predict/scan/permutation_$i/bedGraph_tracks --outDir $tmp_dir_predict/predict/scan/permutation_$i/evaluation";

		system "$hotspot_command" unless -d "$tmp_dir_predict/predict/scan/permutation_$i/evaluation";
	}

	if( $permutation_list[$x - 1] != 1 ){

		system "perl src/utilities/perl/extract_FP_MuStARD_scanning_mode.pl $annotation_file $max_size iteration_$x $tmp_dir_predict/predict/scan $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode";

		system "perl src/utilities/perl/aggregate_evaluate_MuStARD_scanning_mode.pl $tmp_dir_predict/predict/scan $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode";

		my $best_permutation = "NAN";
		my $best_permutation_FP = 1000000000;
		open(IN,"$output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/FP.csv") or die "$!: $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/FP.csv\n";
		while(my $line = <IN>){
			chomp $line;

			#Threshold	File	Value
			#0.5	all.permutation.results/predict/scan/permutation_16	31116

			if( $line =~ /^Threshold/ ){
				next;
			}

			my @temp = split(/\t/,$line);

			if( $temp[0] == 0.5 and $temp[2] < $best_permutation_FP ){

				$temp[1] =~ /predict\/scan\/(permutation_\d{1,})$/;

				$best_permutation = $1;
			}
		}
		close IN;

		print LOGOUT "\n\tGetting FPs\n";

		system "cp $output_dir/iteration_$x/$best_permutation/all.negatives.bed $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode";

		system "mv $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.bed $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.$best_permutation.bed";

		system "bedtools intersect -v -a $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.$best_permutation.bed -b $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.bed > $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.noFPoverlap.bed";

		system "bedtools intersect -v -b $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.$best_permutation.bed -a $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.bed > $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.noPermutationOverlap.bed";

		system "cat $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.noFPoverlap.bed $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.noPermutationOverlap.bed | bedtools sort > $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.new.bed";

		system "bedtools intersect -v -a $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.new.bed -b $annotation_file > $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.negatives.new.no_annotation_overlap.bed";

		my $tmp_command_FP = `wc -l $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.bed`;
		print LOGOUT "\t\tFPs: $tmp_command_FP\n";

		$tmp_command_FP = `wc -l $output_dir/iteration_$x/after.FP.selection/$tmp_input_mode/all.predictions.FP.noPermutationOverlap.bed`;
		print LOGOUT "\t\tFP with no previous negative overlap: $tmp_command_FP\n";

# 		cd /mnt/bigdata/Projects/MuStARD/Data/sequence_fold_conservation_miRNA_hq_random_coding_noncoding_100nt/after.FP.selection/sequence_fold_conservation
# 
# 		perl /home/georgeg/workspace/dev/MuStARD/src/utilities/perl/extract_FP_MuStARD_scanning_mode.pl /mnt/bigdata/DATA/miRBase/v22/hsa/hsa.hairpin.bed 100 /mnt/bigdata/Projects/MuStARD/Jobs/sequence_fold_conservation_miRNA_hq_random_coding_noncoding_100nt_balanced/all.permutation.results/predict/scan .
# 
# 		perl /home/georgeg/workspace/dev/MuStARD/src/utilities/perl/aggregate_evaluate_MuStARD_scanning_mode.pl /mnt/bigdata/Projects/MuStARD/Jobs/sequence_fold_conservation_miRNA_hq_random_coding_noncoding_100nt_balanced/all.permutation.results/predict/scan .
# 
# 		# Identify best permutation from metrics table - permutation_16
# 		cp /mnt/bigdata/Projects/MuStARD/Data/sequence_fold_conservation_miRNA_hq_random_coding_noncoding_100nt/permutation_16/all.negatives.bed .
# 		mv all.negatives.bed all.negatives.permutation_16.bed
# 
# 		bedtools intersect -v -a all.negatives.permutation_16.bed -b all.predictions.FP.bed > all.negatives.permutation_16.noFPoverlap.bed
# 
# 		bedtools intersect -v -b all.negatives.permutation_16.bed -a all.predictions.FP.bed > all.predictions.FP.noPermutationOverlap.bed
# 
# 		cat all.negatives.permutation_16.noFPoverlap.bed all.predictions.FP.noPermutationOverlap.bed | bedtools sort > all.negatives.new.bed
# 
# 		bedtools intersect -v -a all.negatives.new.bed -b /mnt/bigdata/DATA/miRBase/v22/hsa/hsa.hairpin.bed > all.negatives.new.no_miRNA_overlap.bed
	}
}

close LOGOUT;

############## Subroutines
##########################

sub load_ini_file {

	my ($inFile) = @_;

	my %hash;
	open(IN,$inFile) or die "$!: $inFile.\n";
	while(my $line = <IN>){
		chomp $line;

		if( $line =~ /^#/ ){
			next;
		}

		$line =~ /^(.+)=(.+)$/;

		$hash{$1} = $2;
	}
	close IN;

	return \%hash;
}
