use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $window_size = 100;
my $min_threshold = 0.5;
my $max_threshold = 0.9;
my $threshold_step = 0.02;

my ($help, $pred_file, $working_dir);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predFile=s' => \$pred_file,
		'outDir=s' => \$working_dir,
		'minThresh=f' => \$min_threshold,
		'maxThresh=f' => \$max_threshold,
		'threshStep=f' => \$threshold_step,
		'winSize=i' => \$window_size
	)
or defined $help or (!defined $pred_file or !defined $working_dir));

warn "\n\thotspots_MuStARD_scanning_mode in $working_dir with parameters:\n".
	"\t\tPrediction file - $pred_file\n".
	"\t\tMin threshold - $min_threshold\n".
	"\t\tMax threshold - $max_threshold\n".
	"\t\tThreshold step - $threshold_step\n".
	"\t\tWindow size - $window_size\n\n";

system "mkdir $working_dir" unless -d $working_dir;

warn "\t\t# Loading predictions.\n";
my ($predictions, $predictions_unstranded, $predictions_summits, $predictions_unstranded_summits) = merge_nearby_predictions($pred_file, $window_size, $min_threshold, $max_threshold, $threshold_step);

warn "\t\t# Printing hotspots.\n";
foreach my $cutoff (keys %$predictions){

	warn "\t\t\tCutoff at $cutoff.\n";

	open(OUT,">$working_dir/hotspots.stranded.$cutoff.bed") or die "$!: $working_dir/hotspots.stranded.$cutoff.bed.\n";
	open(OUT_SUMMIT,">$working_dir/hotspots.stranded.summits.$cutoff.bed") or die "$!: $working_dir/hotspots.stranded.summits.$cutoff.bed.\n";
	foreach my $chrom ( sort { $a cmp $b } keys(%{$$predictions{$cutoff}})){

		foreach my $strand ( sort { $a cmp $b } keys(%{$$predictions{$cutoff}{$chrom}})){

			@{$$predictions{$cutoff}{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$$predictions{$cutoff}{$chrom}{$strand}};

			for(my $i = 0; $i < @{$$predictions{$cutoff}{$chrom}{$strand}}; $i++){

				my $start = ${${$$predictions{$cutoff}{$chrom}{$strand}}[$i]}[1];
				my $stop = ${${$$predictions{$cutoff}{$chrom}{$strand}}[$i]}[2];
				my $score = ${${$$predictions{$cutoff}{$chrom}{$strand}}[$i]}[3];

				my $name = "prediction_$chrom\_$strand\_$i";

				print OUT "$chrom\t$start\t$stop\t$name\t$score\t$strand\n";
			}

			@{$$predictions_summits{$cutoff}{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$$predictions_summits{$cutoff}{$chrom}{$strand}};

			for(my $i = 0; $i < @{$$predictions_summits{$cutoff}{$chrom}{$strand}}; $i++){

				my $start = ${${$$predictions_summits{$cutoff}{$chrom}{$strand}}[$i]}[1];
				my $stop = ${${$$predictions_summits{$cutoff}{$chrom}{$strand}}[$i]}[2];
				my $score = ${${$$predictions_summits{$cutoff}{$chrom}{$strand}}[$i]}[3];

				my $name = "prediction_$chrom\_$strand\_$i";

				print OUT_SUMMIT "$chrom\t$start\t$stop\t$name\t$score\t$strand\n";
			}
		}
	}
	close OUT;
	close OUT_SUMMIT;

	open(OUT,">$working_dir/hotspots.unstranded.$cutoff.bed") or die "$!: $working_dir/hotspots.unstranded.$cutoff.bed.\n";
	open(OUT_SUMMIT,">$working_dir/hotspots.unstranded.summit.$cutoff.bed") or die "$!: $working_dir/hotspots.unstranded.summit.$cutoff.bed.\n";
	foreach my $chrom ( sort { $a cmp $b } keys(%{$$predictions_unstranded{$cutoff}})){

		@{$$predictions_unstranded{$cutoff}{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$$predictions_unstranded{$cutoff}{$chrom}};

		for(my $i = 0; $i < @{$$predictions_unstranded{$cutoff}{$chrom}}; $i++){

			my $start = ${${$$predictions_unstranded{$cutoff}{$chrom}}[$i]}[1];
			my $stop = ${${$$predictions_unstranded{$cutoff}{$chrom}}[$i]}[2];
			my $score = ${${$$predictions_unstranded{$cutoff}{$chrom}}[$i]}[3];

			my $name = "prediction_$chrom\_$i";

			print OUT "$chrom\t$start\t$stop\t$name\t$score\t.\n";
		}

		@{$$predictions_unstranded_summits{$cutoff}{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$$predictions_unstranded_summits{$cutoff}{$chrom}};

		for(my $i = 0; $i < @{$$predictions_unstranded_summits{$cutoff}{$chrom}}; $i++){

			my $start = ${${$$predictions_unstranded_summits{$cutoff}{$chrom}}[$i]}[1];
			my $stop = ${${$$predictions_unstranded_summits{$cutoff}{$chrom}}[$i]}[2];
			my $score = ${${$$predictions_unstranded_summits{$cutoff}{$chrom}}[$i]}[3];

			my $name = "prediction_$chrom\_$i";

			print OUT_SUMMIT "$chrom\t$start\t$stop\t$name\t$score\t.\n";
		}
	}
	close OUT;
	close OUT_SUMMIT;
}

############################ Subroutines
########################################

sub merge_nearby_predictions {

	my ($predictionFile, $windowSize, $minThreshold, $maxThreshold, $thresholdStep) = @_;

	my (%regions, %regions_unstranded);
	open(IN,"<:gzip",$predictionFile) or die "$!: $predictionFile.\n";
	while(my $line = <IN>){
		chomp $line;

		if( $line =~ /bedGraph/ ){
			next;
		}

		my @temp = split(/\t/,$line);

		my $strand = "+";
		if( $temp[3] < 0 ){
			$strand = "-";
			$temp[3] = abs($temp[3]);
		}

		my $start = int(($temp[1] + $temp[2]) / 2) - int($windowSize / 2);
		my $stop = int(($temp[1] + $temp[2]) / 2) + int($windowSize / 2);

		$temp[1] = $start;
		$temp[2] = $stop;

		for(my $cutoff = $minThreshold; $cutoff <= $maxThreshold; $cutoff += $thresholdStep){

			if( $temp[3] >= $cutoff ){

				my @tmp = ($temp[0], $start, $stop, $temp[3]);
				my @tmp_unstranded = ($temp[0], $start, $stop, $temp[3]);

				push @{$regions{"threshold.$cutoff"}{$temp[0]}{$strand}}, \@tmp;
				push @{$regions_unstranded{"threshold.$cutoff"}{$temp[0]}}, \@tmp_unstranded;
			}
		}
	}
	close IN;

	my (%clusters, %clusters_summits);
	foreach my $cutoff (keys %regions){

		warn "\t\t\tStranded hotspots - $cutoff.\n";

		foreach my $chrom (keys %{$regions{$cutoff}}){

			foreach my $strand (keys %{$regions{$cutoff}{$chrom}}){

				@{$regions{$cutoff}{$chrom}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$regions{$cutoff}{$chrom}{$strand}};

				my $last_index = "empty";
				for(my $i = 0; $i < @{$regions{$cutoff}{$chrom}{$strand}}; $i++){

					if( $last_index eq "empty" ){

						my @tmp = @{${$regions{$cutoff}{$chrom}{$strand}}[$i]};

						push @{$clusters{$cutoff}{$chrom}{$strand}}, \@tmp;

						my @tmp_summit = @{${$regions{$cutoff}{$chrom}{$strand}}[$i]};

						push @{$clusters_summits{$cutoff}{$chrom}{$strand}}, \@tmp_summit;
						$last_index = 0;
					}
					else{

						my $previous_stop = ${${$clusters{$cutoff}{$chrom}{$strand}}[$last_index]}[2];
						my $previous_score = ${${$clusters{$cutoff}{$chrom}{$strand}}[$last_index]}[3];

						my $current_start = ${${$regions{$cutoff}{$chrom}{$strand}}[$i]}[1];
						my $current_score = ${${$regions{$cutoff}{$chrom}{$strand}}[$i]}[3];

						if( $current_start <= $previous_stop ){

							${${$clusters{$cutoff}{$chrom}{$strand}}[$last_index]}[2] = ${${$regions{$cutoff}{$chrom}{$strand}}[$i]}[2];

							if( $current_score > $previous_score ){

								${${$clusters{$cutoff}{$chrom}{$strand}}[$last_index]}[3] = ${${$regions{$cutoff}{$chrom}{$strand}}[$i]}[3];
								@{${$clusters_summits{$cutoff}{$chrom}{$strand}}[$last_index]} = @{${$regions{$cutoff}{$chrom}{$strand}}[$i]};
							}
						}
						else{

							my @tmp = @{${$regions{$cutoff}{$chrom}{$strand}}[$i]};

							push @{$clusters{$cutoff}{$chrom}{$strand}}, \@tmp;

							my @tmp_summit = @{${$regions{$cutoff}{$chrom}{$strand}}[$i]};

							push @{$clusters_summits{$cutoff}{$chrom}{$strand}}, \@tmp_summit;
							$last_index++;
						}
					}
				}
			}
		}
	}

	my (%clusters_unstranded, %clusters_unstranded_summits);
	foreach my $cutoff (keys %regions_unstranded){

		warn "\t\t\tUnstranded hotspots - $cutoff.\n";

		foreach my $chrom (keys %{$regions_unstranded{$cutoff}}){

# 			warn scalar(@{$regions_unstranded{$cutoff}{$chrom}})."\n";

			@{$regions_unstranded{$cutoff}{$chrom}} = sort { $$a[1] <=> $$b[1] } @{$regions_unstranded{$cutoff}{$chrom}};

			my $last_index = "empty";
			for(my $i = 0; $i < @{$regions_unstranded{$cutoff}{$chrom}}; $i++){

# 				warn "$i\n";

				if( $last_index eq "empty" ){

					my @tmp = @{${$regions_unstranded{$cutoff}{$chrom}}[$i]};

					push @{$clusters_unstranded{$cutoff}{$chrom}}, \@tmp;

					my @tmp_summit = @{${$regions_unstranded{$cutoff}{$chrom}}[$i]};

					push @{$clusters_unstranded_summits{$cutoff}{$chrom}}, \@tmp_summit;
					$last_index = 0;
				}
				else{

					my $previous_stop = ${${$clusters_unstranded{$cutoff}{$chrom}}[$last_index]}[2];
					my $previous_score = ${${$clusters_unstranded{$cutoff}{$chrom}}[$last_index]}[3];

					my $current_start = ${${$regions_unstranded{$cutoff}{$chrom}}[$i]}[1];
					my $current_score = ${${$regions_unstranded{$cutoff}{$chrom}}[$i]}[3];

					if( $current_start <= $previous_stop ){

						${${$clusters_unstranded{$cutoff}{$chrom}}[$last_index]}[2] = ${${$regions_unstranded{$cutoff}{$chrom}}[$i]}[2];

						if( $current_score > $previous_score ){

							${${$clusters_unstranded{$cutoff}{$chrom}}[$last_index]}[3] = ${${$regions_unstranded{$cutoff}{$chrom}}[$i]}[3];
							@{${$clusters_unstranded_summits{$cutoff}{$chrom}}[$last_index]} = @{${$regions_unstranded{$cutoff}{$chrom}}[$i]};
						}
					}
					else{

						my @tmp = @{${$regions_unstranded{$cutoff}{$chrom}}[$i]};

						push @{$clusters_unstranded{$cutoff}{$chrom}}, \@tmp;

						my @tmp_summit = @{${$regions_unstranded{$cutoff}{$chrom}}[$i]};

						push @{$clusters_unstranded_summits{$cutoff}{$chrom}}, \@tmp_summit;
						$last_index++;
					}
				}
			}
		}
	}

	return \%clusters,\%clusters_unstranded,\%clusters_summits,\%clusters_unstranded_summits;
}

sub usage {

	print	"\nUsage: hotspots_MuStARD_scanning_mode.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predFile path/to/bedgraph/file\tBedgraph formatted file with predictions.\n".
		"\t--outDir path/to/output/dir\t\tOutput directory for saving the results.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--minThresh [0.5,1)\t\tMinimum threshold to apply on predictions prior to hotspot assembly. Default = 0.5.\n".
		"\t--maxThresh [0.5,1)\t\tMaximum threshold to apply on predictions prior to hotspot assembly. Default = 0.5.\n".
		"\t--threshStep FLOAT\t\tStep that will be applied on threshold cutoff. Default = 0.02.\n".
		"\t--winSize INT\t\t\tWindow size that will be used for calculating the overlaps. Must be equal to the corresponding size used for training. Default = 100.\n";
	exit;
}
