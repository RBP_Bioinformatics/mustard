use warnings;
use strict;

use Getopt::Long;
use Set::IntervalTree;

my $overlap_fraction = 0.1;
my $min_threshold = 0.1;
my $max_threshold = 0.9;
my $stranded = 0;
my $threshold_step = 0.02;

my ($help, $prediction_dir, $annotation_file, $out_prefix);

usage() if ( @ARGV < 1 or join("", @ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'predDir=s' => \$prediction_dir,
		'anotFile=s' => \$annotation_file,
		'outPrefix=s' => \$out_prefix,
		'overlapFraction=f' => \$overlap_fraction,
		'min=f' => \$min_threshold,
		'max=f' => \$max_threshold,
		'threshStep=f' => \$threshold_step,
		'stranded=i' => \$stranded,
	)
or defined $help or (!defined $prediction_dir or !defined $annotation_file or !defined $out_prefix));

warn "\n\tevaluate_algorithm in $out_prefix with parameters:\n".
	"\t\tPrediction dir - $prediction_dir\n".
	"\t\tAnnotation file - $annotation_file\n".
	"\t\tMin threshold - $min_threshold\n".
	"\t\tMax threshold - $max_threshold\n".
	"\t\tThreshold step - $threshold_step\n".
	"\t\tStranded - $stranded\n".
	"\t\tOverlap fraction - $overlap_fraction\n\n";

my $OUT;
if( $stranded ){
	open($OUT,">$out_prefix.stranded.csv") or die "$!: $out_prefix.stranded.csv\n";
}
else{
	open($OUT,">$out_prefix.unstranded.csv") or die "$!: $out_prefix.unstranded.csv\n";
}

print $OUT "threshold\tpositives\ttotal\tTP\tFP\tTN\tFN\taccuracy\tprecision\tsensitivity\tspecificity\tspecificityR\n";
for(my $j = $min_threshold; $j <= $max_threshold; $j += $threshold_step ){

	my $prediction_file = "$prediction_dir/hotspots.unstranded.threshold.$j.bed";
	if( $stranded ){
		$prediction_file = "$prediction_dir/hotspots.stranded.threshold.$j.bed";
	}

	unless( -e $prediction_file ){
		next;
	}

	my $predictions = load_predictions($prediction_file, $stranded);

	my ($annotated_regions_IT,$total_positives) = load_annotations($annotation_file, $stranded);

	my $TP = 0;
	my $FP = 0;
	my $TN = 0;
	my $FN = 0;
	my %identified_loci;
	foreach my $chrom (keys %$predictions){

		if( $stranded ){

			foreach my $strand (keys %{$$predictions{$chrom}}){

				for(my $i = 0; $i < @{$$predictions{$chrom}{$strand}}; $i++){

					my $start = ${${$$predictions{$chrom}{$strand}}[$i]}[1];
					my $stop = ${${$$predictions{$chrom}{$strand}}[$i]}[2];
					my $score = ${${$$predictions{$chrom}{$strand}}[$i]}[4];
# 					die "$start\t$stop\t$score\n";

					my $overlap_flag = 0;
					if( exists $$annotated_regions_IT{$chrom}{$strand} ){

						my $results = $$annotated_regions_IT{$chrom}{$strand}->fetch($start, $stop);

					
						if( $overlap_fraction == 0 ){

							if( scalar(@$results) >= 1 ){

								for(my $index = 0; $index < scalar(@$results); $index++){

									$$results[$index] =~ /^(.+)__.+__.+$/;
									$identified_loci{$1} = 1;
								}

								$overlap_flag = 1;
							}
						}
						else{

							if( scalar(@$results) >= 1 ){

								my $at_least_once = 0;
								for(my $index = 0; $index < scalar(@$results); $index++){

									$$results[$index] =~ /^(.+)__(.+)__(.+)$/;

									my @tmp = ($start, $stop, $2, $3);
									@tmp = sort { $a <=> $b } @tmp;

									if( ($tmp[2] - $tmp[1]) / ($3 - $2) >= $overlap_fraction ){

										$at_least_once = 1;
										$identified_loci{$1} = 1;
									}
								}

								if( $at_least_once ){
									$overlap_flag = 1;
								}
							}
						}
					}

					if( $overlap_flag == 0 ){

						if( $score >= $j ){

							$FP++;
						}
						else{

							$TN++;
						}
					}
					else{

						if( $score >= $j ){

							$TP++;
						}
						else{

							$FN++;
						}
					}
				}
			}
		}
		else{

			for(my $i = 0; $i < @{$$predictions{$chrom}}; $i++){

				my $start = ${${$$predictions{$chrom}}[$i]}[1];
				my $stop = ${${$$predictions{$chrom}}[$i]}[2];
				my $score = ${${$$predictions{$chrom}}[$i]}[4];
# 				die "$start\t$stop\t$score\n";

				my $overlap_flag = 0;

				if( exists $$annotated_regions_IT{$chrom} ){

					my $results = $$annotated_regions_IT{$chrom}->fetch($start, $stop);

					if( $overlap_fraction == 0 ){

						if( scalar(@$results) >= 1 ){

							for(my $index = 0; $index < scalar(@$results); $index++){

								$$results[$index] =~ /^(.+)__.+__.+$/;
								$identified_loci{$1} = 1;
							}

							$overlap_flag = 1;
						}
					}
					else{

						if( scalar(@$results) >= 1 ){

							my $at_least_once = 0;
							for(my $index = 0; $index < scalar(@$results); $index++){

								$$results[$index] =~ /^(.+)__(.+)__(.+)$/;

								my @tmp = ($start, $stop, $2, $3);
								@tmp = sort { $a <=> $b } @tmp;

								if( ($tmp[2] - $tmp[1]) / ($3 - $2) >= $overlap_fraction ){

									$at_least_once = 1;
									$identified_loci{$1} = 1;
# 									die "$1\n";
								}
							}

							if( $at_least_once ){
								$overlap_flag = 1;
							}
						}
					}
				}

				if( $overlap_flag == 0 ){

					if( $score >= $j ){

						$FP++;
					}
					else{

						$TN++;
					}
				}
				else{

					if( $score >= $j ){

						$TP++;
					}
					else{

						$FN++;
					}
				}
			}
		}
	}

	my $accuracy = ($TP + $TN) / ($TP + $TN + $FP + $FN);
	my $precision = $TP / ($TP + $FP);
	my $sensitivity = 0;
	my $specificity = 0;

	if( $min_threshold == 0.1 ){
		$sensitivity = scalar(keys(%identified_loci)) / $total_positives;
# 		warn scalar(keys(%identified_loci))."\t".$total_positives."\t".$sensitivity."\n";
		$specificity = 0;
	}
	else{

		$sensitivity = $TP / ($TP + $FN);
		$specificity = $TN / ($TN + $FP);
	}

	print $OUT "$j\t$total_positives\t".($TP + $TN + $FP + $FN)."\t$TP\t$FP\t$TN\t$FN\t$accuracy\t$precision\t$sensitivity\t$specificity\t".(1-$specificity)."\n";
}
close $OUT;


############################ Subroutines

sub load_predictions {

	my ($predictionFile, $strand_mode) = @_;

	my %regions;
	open(IN,$predictionFile) or die "$!: $predictionFile.\n";
	while(my $line = <IN>){
		chomp $line;

# 		die "$line\n";
		my @temp = split(/\t/,$line);

		if( $strand_mode ){

			push @{$regions{$temp[0]}{$temp[5]}}, \@temp;
		}
		else{

			push @{$regions{$temp[0]}}, \@temp;
		}
	}
	close IN;

	return \%regions;
}

sub load_annotations {

	my ($inputFile, $strand_mode) = @_;

	my (%annotated_loci_IT,$counter);
	open(IN,$inputFile) or die "$!: $inputFile.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

		unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
			next;
		}

		if( $strand_mode ){

			unless( exists $annotated_loci_IT{$temp[0]}{$temp[5]} ){

				$annotated_loci_IT{$temp[0]}{$temp[5]} = Set::IntervalTree->new;
			}
		}
		else{

			unless( exists $annotated_loci_IT{$temp[0]} ){

				$annotated_loci_IT{$temp[0]} = Set::IntervalTree->new;
			}
		}

		my $tmp_name = "$temp[3]__$temp[1]__$temp[2]";

# 		die "$temp[3]\t$temp[1]\t$temp[2]\n";

		if( $strand_mode ){

			$annotated_loci_IT{$temp[0]}{$temp[5]}->insert($tmp_name,$temp[1],$temp[2]);
		}
		else{

			$annotated_loci_IT{$temp[0]}->insert($tmp_name,$temp[1],$temp[2]);
		}

		$counter++;
	}
	close IN;

	return \%annotated_loci_IT,$counter;
}

sub usage {

	print	"\nUsage: evaluate_algorithm.pl [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--predDir path/to/bed/dir\t\tDirectory with bed formatted predictions.\n".
		"\t--anotFile path/to/annotation/file\tClass specific annotation file that will be used as ground truth.\n".
		"\t--outPrefix /output/prefix\t\tPrefix that will be used to generate files with the results.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\tPrint this help message.\n".
		"\t--min (0,1)\t\t\tMinimum score threshold. Default = 0.1.\n".
		"\t--max (0,1)\t\t\tMaximum score threshold. Default = 0.9.\n".
		"\t--threshStep FLOAT\t\tStep that will be applied on threshold cutoff. Default = 0.02.\n".
		"\t--stranded [0|1]\t\t\tEvaluate based on strand. Default = 0, no strand usage.\n".
		"\t--overlapFraction (0,1]\t\tFraction of overlap between predictions and annotation. Default = 0.1. Use 0 for at least 1 nt overlap.\n";
	exit;
}
