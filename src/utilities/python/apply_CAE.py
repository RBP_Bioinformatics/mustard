import numpy as np
import sys
import gzip
from itertools import islice
import os
import re
from collections import defaultdict

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras import models as km

pathname = os.path.abspath(os.path.dirname(sys.argv[0])) + "/../../lib/python"
sys.path.insert(0, pathname)
from Files import Format


def main():

	model_file = sys.argv[1]
	input_prefix = sys.argv[2]
	output_prefix = sys.argv[3]
	input_mode = sys.argv[4]

	#print(input_file)
	#print(model_file)
	#print(file_prefix)
	#sys.exit()

	model = km.load_model(model_file)

	if os.path.isfile(output_prefix + '.predictions.txt'):
		os.remove(output_prefix + '.predictions.txt')

	one_hot_samples = []

	modes = input_mode.split(",")
	for mode in modes:

		input_file = input_prefix + "." + mode + ".tab"

		one_hot_samples.append(Format.to_one_hot(input_file, mode, 'predict', 'unweighted'))

	prediction_scores = model.predict(one_hot_samples)

	mse_list = []
	for i in range(len(modes)):

		tmp_one_hot_samples = one_hot_samples[i]
		tmp_prediction_scores = prediction_scores[i]
		#print tmp_one_hot_samples.shape
		#print tmp_prediction_scores.shape

		if modes[i] != 'conservation':
			tmp_one_hot_samples = tmp_one_hot_samples.reshape((tmp_one_hot_samples.shape[0], -1))
			tmp_prediction_scores = tmp_prediction_scores.reshape((tmp_prediction_scores.shape[0], -1))
		#print tmp_one_hot_samples.shape
		#print tmp_prediction_scores.shape

		mse = np.mean(np.power(tmp_one_hot_samples - tmp_prediction_scores, 2), axis=1)
		#print mse.shape

		mse_list.append(mse)
	#print mse_list
	#sys.exit()

	placeholder = mse_list[0]

	out_vector = np.zeros((placeholder.shape[0],len(modes)),dtype=np.float32)

	for i in range(out_vector.shape[0]):

		for j in range(out_vector.shape[1]):

			tmp_mse_list = mse_list[j]
			out_vector[i,j] = tmp_mse_list[i]

	fh_predictions = open(output_prefix + '.predictions.txt', 'w')
	np.savetxt(fh_predictions, np.sum(out_vector, axis=1), delimiter="\t")
	fh_predictions.close()

	gzip_command = 'gzip ' + output_prefix + '.predictions.txt'
	os.system(gzip_command)

	#fh_sense = open(file_prefix + '.predictions.txt', 'w')
	#chunk_size = 10000
	#with gzip.open(input_file,'r') as file_handle:

		#for chunk_size_lines in iter(lambda: tuple(islice(file_handle, chunk_size)), ()):

			#sequences_one_hot_sense = []
			#if input_mode == 'sequence':
				#sequences_one_hot_sense = Format.convert_sequence_batch_to_one_hot(chunk_size_lines, sequence_dim)
			#elif input_mode == 'fold':
				#sequences_one_hot_sense = Format.convert_fold_batch_to_one_hot(chunk_size_lines, sequence_dim)

			#predictions_sense = model.predict(sequences_one_hot_sense)

			#np.savetxt(fh_sense, predictions_sense, delimiter="\t")

			##for line in chunk_size_lines:

				###print line
				###sys.exit()

				##line = line.strip("\n")
				##split_line = line.split()

				##sequence_one_hot_sense, sequence_one_hot_antisense = Format.convert_sequence_to_one_hot(split_line[1])

				##prediction_sense = model.predict(sequence_one_hot_sense)
				##prediction_antisense = model.predict(sequence_one_hot_antisense)

				##np.savetxt(fh_sense, prediction_sense, delimiter="\t")
				##np.savetxt(fh_antisense, prediction_antisense, delimiter="\t")
	#fh_sense.close()

	#gzip_command = 'gzip ' + file_prefix + '.predictions.txt'
	#os.system(gzip_command)


if __name__ == '__main__':
	main()
