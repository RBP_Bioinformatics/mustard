import numpy as np
import sys
import gzip
from itertools import islice
import os
import re
from collections import defaultdict

#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras import models as km

pathname = os.path.abspath(os.path.dirname(sys.argv[0])) + "/../../lib/python"
sys.path.insert(0, pathname)
from Files import Format


def main():

	model_file = sys.argv[1]
	input_prefix = sys.argv[2]
	output_prefix = sys.argv[3]
	input_mode = sys.argv[4]

	#print(input_file)
	#print(model_file)
	#print(file_prefix)
	#sys.exit()

	# Report the GPU device information, if present
	sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

	model = km.load_model(model_file)

	if os.path.isfile(output_prefix + '.predictions.txt'):
		os.remove(output_prefix + '.predictions.txt')

	one_hot_samples = []

	modes = input_mode.split(",")
	for mode in modes:

		input_file = input_prefix + "." + mode + ".tab.gz"

		one_hot_samples.append(Format.to_one_hot(input_file, mode, 'predict', 'unweighted'))

	fh_predictions = open(output_prefix + '.predictions.txt', 'w')
	prediction_scores = model.predict(one_hot_samples)
	np.savetxt(fh_predictions, prediction_scores, delimiter="\t")
	fh_predictions.close()

	gzip_command = 'gzip ' + output_prefix + '.predictions.txt'
	os.system(gzip_command)

	#fh_sense = open(file_prefix + '.predictions.txt', 'w')
	#chunk_size = 10000
	#with gzip.open(input_file,'r') as file_handle:

		#for chunk_size_lines in iter(lambda: tuple(islice(file_handle, chunk_size)), ()):

			#sequences_one_hot_sense = []
			#if input_mode == 'sequence':
				#sequences_one_hot_sense = Format.convert_sequence_batch_to_one_hot(chunk_size_lines, sequence_dim)
			#elif input_mode == 'fold':
				#sequences_one_hot_sense = Format.convert_fold_batch_to_one_hot(chunk_size_lines, sequence_dim)

			#predictions_sense = model.predict(sequences_one_hot_sense)

			#np.savetxt(fh_sense, predictions_sense, delimiter="\t")

			##for line in chunk_size_lines:

				###print line
				###sys.exit()

				##line = line.strip("\n")
				##split_line = line.split()

				##sequence_one_hot_sense, sequence_one_hot_antisense = Format.convert_sequence_to_one_hot(split_line[1])

				##prediction_sense = model.predict(sequence_one_hot_sense)
				##prediction_antisense = model.predict(sequence_one_hot_antisense)

				##np.savetxt(fh_sense, prediction_sense, delimiter="\t")
				##np.savetxt(fh_antisense, prediction_antisense, delimiter="\t")
	#fh_sense.close()

	#gzip_command = 'gzip ' + file_prefix + '.predictions.txt'
	#os.system(gzip_command)


if __name__ == '__main__':
	main()
