import sys
import numpy as np
import os
import matplotlib.pyplot as plt
import random as rn

#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
os.environ['PYTHONHASHSEED'] = '0'

# Necessary for starting Numpy generated random numbers in a well-defined initial state.
np.random.seed(20)

# Necessary for starting core Python generated random numbers in a well-defined state.
rn.seed(1984)

import keras
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Conv1D, MaxPooling1D, Activation, LSTM, Bidirectional, Concatenate, Input, BatchNormalization, Lambda, Reshape, UpSampling1D
from keras.optimizers import SGD, RMSprop, Adam
from keras import metrics
from keras import backend as K
from keras.layers.advanced_activations import LeakyReLU
from keras import regularizers
from keras import initializers

# Will make random number generation in the TensorFlow backend have a well-defined initial state.
tf.set_random_seed(8)

# Import MuStARD specific library
pathname = os.path.abspath(os.path.dirname(sys.argv[0])) + "/../../lib/python"
sys.path.insert(0, pathname)
from Files import Format

def main():

	input_directory = sys.argv[1]
	output_directory = sys.argv[2]
	input_mode = sys.argv[3]

	# Report the GPU device information, if present
	sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))

	print "\n\t\t\tConverting data to one-hot encoding."

	the_one = dict()
	the_one["train"] = dict()
	the_one["test"] = dict()
	the_one["valid"] = dict()
	modes = input_mode.split(",")
	for mode in modes:

		train_file = input_directory + "/train." + mode + ".tab"
		test_file = input_directory + "/test." + mode + ".tab"
		valid_file = input_directory + "/validation." + mode + ".tab"

		print "\t\t\tLoading train data - " + mode
		the_one["train"][mode], the_one["train"]["labels"], the_one["train"]["l_dim"], the_one["train"]["seq_dim"] = Format.to_one_hot(train_file, mode, "train", "unweighted")
		#the_one["train"][mode], the_one["train"]["labels"], the_one["train"]["l_dim"], the_one["train"]["seq_dim"], the_one["train"]["wghts"] = Format.to_one_hot(train_file, mode, "train", "weighted")

		print "\t\t\tLoading test data - " + mode
		the_one["test"][mode], the_one["test"]["labels"], the_one["test"]["l_dim"], the_one["test"]["seq_dim"] = Format.to_one_hot(test_file, mode, "train", "unweighted")

		print "\t\t\tLoading validation data - " + mode
		the_one["valid"][mode], the_one["valid"]["labels"], the_one["valid"]["l_dim"], the_one["valid"]["seq_dim"] = Format.to_one_hot(valid_file, mode, "train", "unweighted")

	output_directory = output_directory + "/" + "_".join(modes)
	if not os.path.exists(output_directory):
		os.mkdir(output_directory, 0755)

	# Lists with multiple values of each hyperparameter to be used in a grid-search type of approach of optimization
	batch_size_list = [1024]
	dropout_list = [0.25]
	lr_list = [0.0001]
	filter_num_list = [80]

	# Loops that will apply the same architecture on different combinations of hyperparameters and save the results in different directories
	for tmp_batch_size in batch_size_list:

		for tmp_dropout in dropout_list:

			for tmp_lr in lr_list:

				for tmp_filter_num in filter_num_list:

					tmp_output_directory = output_directory + "/batch" + str(tmp_batch_size) + "_dropout" + str(tmp_dropout) + "_lr" + str(tmp_lr) + "_filters" + str(tmp_filter_num)
					if not os.path.exists(tmp_output_directory):
						os.mkdir(tmp_output_directory, 0755)
					else:

						print "\n\t\t\t############################################################################################"
						print "\t\t\tSKIPPING Training with params - batch: " + str(tmp_batch_size) + " dropout: " + str(tmp_dropout) + " lr: " + str(tmp_lr) + " filters: " + str(tmp_filter_num)
						print "\t\t\t############################################################################################"
						continue

					print "\n\t\t\t############################################################################################"
					print "\t\t\tTraining with params - batch: " + str(tmp_batch_size) + " dropout: " + str(tmp_dropout) + " lr: " + str(tmp_lr) + " filters: " + str(tmp_filter_num)
					print "\t\t\t############################################################################################"

					call_model(the_one, modes, tmp_batch_size, tmp_dropout, tmp_lr, tmp_filter_num, tmp_output_directory)

					K.clear_session()


def sampling(args):
	"""Reparameterization trick by sampling fr an isotropic unit Gaussian.
	# Arguments:
		args (tensor): mean and log of variance of Q(z|X)
	# Returns:
		z (tensor): sampled latent vector
	"""

	z_mean, z_log_var = args

	batch = K.shape(z_mean)[0]
	dim = K.int_shape(z_mean)[1]

	# by default, random_normal has mean=0 and std=1.0
	epsilon = K.random_normal(shape=(batch, dim))

	return z_mean + K.exp(0.5 * z_log_var) * epsilon


def sequence_branch(sequence_input, tmp_filter_num, tmp_dropout):

	# Encoder
	x = Conv1D(filters = tmp_filter_num, kernel_size = 16, strides = 1, padding = "same")(sequence_input)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 8, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	encoded = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)

	# Decoder
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 8, strides = 1, padding = "same")(encoded)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	x = Conv1D(filters = tmp_filter_num, kernel_size = 16, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	decoded = Conv1D(filters = 4, kernel_size = 16, strides = 1, padding = "same", activation = "sigmoid", name = "sequence")(x)

	return decoded


def fold_branch(fold_input, tmp_filter_num, tmp_dropout):

	# Encoder
	x = Conv1D(filters = tmp_filter_num, kernel_size = 30, strides = 1, padding = "same")(fold_input)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 15, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	encoded = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)

	# Decoder
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 15, strides = 1, padding = "same")(encoded)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	x = Conv1D(filters = tmp_filter_num, kernel_size = 30, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	decoded = Conv1D(filters = 3, kernel_size = 30, strides = 1, padding = "same", activation = "sigmoid", name = "fold")(x)

	return decoded


def conservation_branch(conservation_input, tmp_filter_num, tmp_dropout):

	# Encoder
	x = Conv1D(filters = tmp_filter_num, kernel_size = 20, strides = 1, padding = "same")(conservation_input)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 10, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	#x = BatchNormalization()(x)
	x = MaxPooling1D(pool_size = 2, padding = "same")(x)
	encoded = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)

	# Decoder
	x = Conv1D(filters = int(tmp_filter_num / 2), kernel_size = 10, strides = 1, padding = "same")(encoded)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	x = Conv1D(filters = tmp_filter_num, kernel_size = 20, strides = 1, padding = "same")(x)
	x = LeakyReLU()(x)
	x = Dropout(rate = tmp_dropout, noise_shape = None, seed = None)(x)
	x = UpSampling1D(size = 2)(x)
	decoded = Conv1D(filters = 1, kernel_size = 20, strides = 1, padding = "same", activation = "linear", name = "conservation")(x)

	return decoded


def call_model(the_one, modes, tmp_batch_size, tmp_dropout, tmp_lr, tmp_filter_num, tmp_output_directory):
	"""This function assembles the model and performs the training.
	The architecture example in this function uses two different convolutional layers with shared parameters.
	One layer is applied on the forward strand and the second one on the reverse strand.
	The output of these layers is concatenated before forwarded to the biderectional LSTM layer.
	"""

	sequence_inputs = []
	sequence_outputs = []

	label_dim = the_one["train"]["l_dim"]
	sequence_dim = the_one["train"]["seq_dim"]

	train_x = []
	train_y = the_one["train"]["labels"]
	test_x = []
	test_y = the_one["test"]["labels"]
	valid_x = []
	valid_y = the_one["valid"]["labels"]

	#print label_dim
	#print sequence_dim
	#print train_y
	#print test_y
	#print valid_y
	#sys.exit()

	for mode in modes:

		if mode == "sequence":

			sequence_input = Input(shape = (sequence_dim, 4))

			sequence_output = sequence_branch(sequence_input, tmp_filter_num, tmp_dropout)

			sequence_inputs.append(sequence_input)
			sequence_outputs.append(sequence_output)

		elif mode == "RNAfold":

			fold_input = Input(shape = (sequence_dim, 3))

			fold_output = fold_branch(fold_input, tmp_filter_num, tmp_dropout)

			sequence_inputs.append(fold_input)
			sequence_outputs.append(fold_output)

		elif mode == "conservation":

			conservation_input = Input(shape = (sequence_dim, 1))

			conservation_output = conservation_branch(conservation_input, tmp_filter_num, tmp_dropout)

			sequence_inputs.append(conservation_input)
			sequence_outputs.append(conservation_output)

		train_x.append(the_one["train"][mode])
		test_x.append(the_one["test"][mode])
		valid_x.append(the_one["valid"][mode])

	print "\t\t\tConnecting network components."

	autoencoder_model = Model(sequence_inputs, sequence_outputs)

	print "\t\t\tCompiling network components."
	sgd = SGD(lr = tmp_lr,
			decay = 1e-6,
			momentum = 0.9,
			#clipnorm = 1.,
			#clipvalue = 0.5,
			nesterov = True)
	#rmsprop = RMSprop(lr=0.01)
	#adam = Adam(lr = 0.01)
	autoencoder_model.compile(optimizer = sgd,
			loss = "mse")
			#metrics = ["mse"])

	#model.summary()
	#sys.exit()

	mcp = ModelCheckpoint(filepath = tmp_output_directory + "/CNNonRaw.hdf5",
				verbose = 0,
				save_best_only = True)
	earlystopper = EarlyStopping(monitor = 'val_loss', 
					patience = 40,
					min_delta = 0,
					verbose = 1,
					mode = 'auto')
	csv_logger = CSVLogger(tmp_output_directory + "/CNNonRaw.log.csv", 
				append=True, 
				separator='\t')

	print "\t\t\tTraining network."
	history = autoencoder_model.fit(train_x, train_x,
			batch_size = tmp_batch_size,
			epochs = 600,
			verbose = 1,
			validation_data = (valid_x, valid_x),
			callbacks = [mcp, earlystopper, csv_logger])

	print "\t\t\tTesting network."
	tresults = autoencoder_model.evaluate(test_x, test_x,
			batch_size = tmp_batch_size,
			verbose = 1,
			sample_weight = None)
	print "\t\t\t[loss]"
	print tresults

	# summarize history for loss
	plt.plot(history.history['loss'])
	plt.plot(history.history['val_loss'])
	plt.ylim(0.0, max(max(history.history['loss']), max(history.history['val_loss'])))
	plt.title('Model Loss')
	plt.ylabel('Categorical Crossentropy')
	plt.xlabel('Epoch')
	plt.legend(['Train', 'Validation'], loc='upper right')
	plt.savefig(tmp_output_directory + "/CNNonRaw.loss.png", dpi=300)
	plt.clf()


if __name__ == '__main__':
	main()
