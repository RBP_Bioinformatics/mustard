use warnings;
use strict;

use Getopt::Long;
use FindBin;
use lib "$FindBin::Bin/lib/perl/";
use Files::MakeSets;
use Files::CleanUp;
use Files::Format;
use Models::DNN;
use Generic::FuS;

my $max_size = 200;
my $extend_flag = 0;
my $shuffled_class_flag = 0;
my $reinforced_number = 5;
my $excluded_chroms_test = "chr1,chr3";
my $excluded_chroms_valid = "chr2,chr4";
my $class_list = "auto";
my $inputMode = "sequence";
my $model_type = "CNN";
my $thread_num = 10;

my ($help, $loci_list, $working_dir, $genome_file, $conservation_dir);

usage() if ( @ARGV < 1 or join("",@ARGV) !~ /-/ or
          ! GetOptions(
		'help|h' => \$help,
		'list=s' => \$loci_list,
		'class=s' => \$class_list,
		'dir=s' => \$working_dir,
		'extFlag=i' => \$extend_flag,
		'reinfNum=i' => \$reinforced_number,
		'shufClassFlag=i' => \$shuffled_class_flag,
		'maxSize=i' => \$max_size,
		'inputMode=s' => \$inputMode,
		'exclTest=s' => \$excluded_chroms_test,
		'exclValid=s' => \$excluded_chroms_valid,
		'genome=s' => \$genome_file,
		'consDir=s' => \$conservation_dir,
		'modelType=s' => \$model_type,
		'threads=s' => \$thread_num
	)
or defined $help or (!defined $loci_list or !defined $working_dir or !defined $genome_file or !defined $conservation_dir));

warn "\n\tMuStARD_train in $working_dir with parameters:\n".
	"\t\tPeak list - $loci_list\n".
	"\t\tClass list - $class_list\n".
	"\t\tGenome file - $genome_file\n".
	"\t\tConservation dir - $conservation_dir\n".
	"\t\tMax size - $max_size\n".
	"\t\tReinforced number - $reinforced_number\n".
	"\t\tExtend flag - $extend_flag\n".
	"\t\tShuffled class flag - $shuffled_class_flag\n".
	"\t\tInput mode - $inputMode\n".
	"\t\tModel type - $model_type\n".
	"\t\tThread number - $thread_num\n".
	"\t\tExcluded chromosome list for testing - $excluded_chroms_test\n".
	"\t\tExcluded chromosome list for validation - $excluded_chroms_valid\n\n";

system "mkdir $working_dir" unless -d $working_dir;
system "mkdir $working_dir/Data" unless -d "$working_dir/Data";

warn "\t\tCreating train, test and validation sets.\n";
Files::MakeSets::make_sets($loci_list, $class_list, $excluded_chroms_test, $excluded_chroms_valid, $max_size, "$working_dir/Data", $extend_flag, $reinforced_number);

warn "\t\tGetting sequences and reformatting files.\n";
Files::CleanUp::finalize_sequence_files("$working_dir/Data", "test,train,validation", $genome_file, $max_size, $extend_flag, $shuffled_class_flag, scalar(split(/,/,$loci_list)));
Files::CleanUp::finalize_files("$working_dir/Data", "test,train,validation", $inputMode, $conservation_dir, $thread_num) unless $inputMode eq "sequence";

system "mkdir $working_dir/Models" unless -d "$working_dir/Models";
warn "\t\tTraining CNN on raw sequences.\n";
Models::DNN::train_DNN("$working_dir/Data", "$working_dir/Models", "$FindBin::Bin/utilities/python", $inputMode, $model_type);

sub usage {

	print	"\nUsage: MuStARD.pl train [Optional Arguments] [Mandatory Arguments]\n".
		"\n\t[Mandatory Arguments]\n".
		"\t--list peak_file_1,peak_file_2,...\tList of peak files (at least 2) to be used as different classes for training.\n".
		"\t--genome /path/to/genome.fa\t\tGenome fasta file corresponding to the reference genome of the peak list.\n".
		"\t--consDir /path/to/cons/dir\t\tDirectory with conservation files in wig.gz format. If inputMode doesn't include conservation, put NaN here.\n".
		"\t--dir working/dir/Jobs/job_...\t\tWorking directory for the training process.\n".
		"\n\t[Optional Arguments]\n".
		"\t--help|-h\t\t\t\t\tPrint this help message.\n".
		"\t--class 1,2,...\t\t\t\t\tList of class labels corresponding to the list of --list|-l argument. Default is auto.\n".
		"\t--maxSize INT\t\t\t\t\tMaximum size of intervals to keep. Default = 200.\n".
		"\t--reinfNum INT\t\t\t\t\tNumber of reinforced amplification of target sequences to be generated. Default = 5.\n".
		"\t--extFlag [0,1]\t\t\t\t\tFlag indicating whether to extend intervals up to maxSize or apply zero padding. Default = 0 (zero padding enabled).\n".
		"\t--shufClassFlag [0,1]\t\t\t\tFlag indicating whether to add a random class by shuffling target sequences. Default = 0 (shuffling disabled).\n".
		"\t--inputMode [sequence|RNAfold|conservation]\tComma separated list of input data. Could be sequence,RNAfold or RNAfold,sequence or sequence,RNAfold,conservation etc. Default = sequence.\n".
		"\t--modelType [CNN|CAE|CVAE]\t\t\tType of architecture/model to use. Default = CNN.\n".
		"\t--threads INT\t\t\t\t\tNumber of threads to use for pre-processing. Default = 10.\n".
		"\t--exclTest chr1,chr3,...\t\t\tComma separated list of chromosomes to exclude from training, kept for testing the model. Default = chr1,chr3.\n".
		"\t--exclValid chr2,chr4,...\t\t\tComma separated list of chromosomes to exclude from training, kept for validating the model. Default = chr2,chr4.\n";
	exit;
}
