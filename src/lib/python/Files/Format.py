import numpy as np
import gzip
import sys

def to_one_hot(input_file, input_mode, program_mode, weight_mode):

	if program_mode == "train":

		if weight_mode == "weighted":

			one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim, class_weights_dict = file_sample_to_one_hot_weighted(input_file, input_mode)
			return one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim, class_weights_dict

		elif weight_mode == "unweighted":

			one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim = file_sample_to_one_hot_unweighted(input_file, input_mode)
			return one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim

	elif program_mode == "predict":

		one_hot_samples = file_sample_to_one_hot_predict(input_file, input_mode)
		return one_hot_samples


def file_sample_to_one_hot_weighted(input_file, input_mode):
	"""
	Takes a tabular 6 column file as input and converts
	to the appropriate one-hot vector representation.
	Class weights are also calculated and returned.
	"""

	input_samples = []
	input_sample_labels = []
	input_sample_dim = 0
	input_sample_label_dim = 0
	total_samples = 0
	class_counter = dict()

	file_handle = gzip.open(input_file,'r')
	for line in file_handle:

		line = line.strip("\n")
		split_line = line.split()
		#print line
		#sys.exit()

		input_samples.append(split_line[5])
		input_sample_labels.append(split_line[4])

		if input_sample_dim == 0:

			if input_mode == "conservation":
				tmp_input_sample = split_line[5]
				tmp_input_sample_splitted = tmp_input_sample.split("_")
				input_sample_dim = len(tmp_input_sample_splitted)
			else:
				input_sample_dim = len(split_line[5])

		tmp_label = split_line[4]
		input_sample_label_dim = len(tmp_label.split("_"))
		tmp_label_splitted = tmp_label.split("_")
		#print tmp_label_splitted
		#sys.exit()
		for i in range(len(tmp_label_splitted)):
			if tmp_label_splitted[i] == '1':
				if i not in class_counter:
					class_counter[i] = 1
				else:
					class_counter[i] += 1
				#print str(class_counter[i])
				#sys.exit()
		total_samples += 1
	file_handle.close()

	class_weights_dict = dict()
	for i in class_counter:
		#class_weights_dict[int(i)] = 1 - (float(class_counter[i]) / float(total_samples))
		class_weights_dict[int(i)] = (float(max(class_counter.values())) / float(class_counter[i]))
		print "\t\t\t\tClass " + str(i) + "\t" + str(class_counter[i]) + "\t" + str(total_samples) + "\t" + str(class_weights_dict[int(i)])

	one_hot_samples = np.zeros((len(input_samples),input_sample_dim,4),dtype=np.float32)
	if input_mode == "RNAfold":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,3),dtype=np.float32)
	elif input_mode == "conservation":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,1),dtype=np.float32)

	one_hot_labels = np.zeros((len(input_sample_labels),input_sample_label_dim),dtype=np.float32)

	for row_num in range(len(input_samples)):

		input_sample = input_samples[row_num]

		if input_mode == "conservation":

			input_sample_splitted = input_sample.split("_")

			for col_num in range(len(input_sample_splitted)):
				one_hot_samples[row_num,col_num,0] = np.float32(input_sample_splitted[col_num])

		else:

			for col_num in range(len(input_sample)):

				if input_mode == "sequence":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_sequence(input_sample[col_num])
				elif input_mode == "RNAfold":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_RNAfold(input_sample[col_num])

		tmp_label = input_sample_labels[row_num]
		one_hot_labels[row_num,:] = np.asarray(tmp_label.split("_"),dtype=np.float32)

		#print input_sample
		#print one_hot_samples[row_num,:,:]

		#print tmp_label
		#print one_hot_labels[row_num,:]
		#sys.exit()

	return one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim, class_weights_dict


def file_sample_to_one_hot_unweighted(input_file, input_mode):
	"""
	Takes a tabular 6 column file as input and converts
	to the appropriate one-hot vector representation.
	No class weights are calculated.
	"""

	input_samples = []
	input_sample_labels = []
	input_sample_dim = 0
	input_sample_label_dim = 0
	class_counter = dict()

	file_handle = gzip.open(input_file,'r')
	for line in file_handle:

		line = line.strip("\n")
		split_line = line.split()

		input_sample_labels.append(split_line[4])
		input_samples.append(split_line[5])

		if input_sample_dim == 0:

			if input_mode == "conservation":
				tmp_input_sample = split_line[5]
				tmp_input_sample_splitted = tmp_input_sample.split("_")
				input_sample_dim = len(tmp_input_sample_splitted)
			else:
				input_sample_dim = len(split_line[5])

		tmp_label = split_line[4]
		input_sample_label_dim = len(tmp_label.split("_"))
		tmp_label_splitted = tmp_label.split("_")
		#print tmp_label_splitted
		#sys.exit()
		for i in range(len(tmp_label_splitted)):
			if tmp_label_splitted[i] == '1':
				if i not in class_counter:
					class_counter[i] = 1
				else:
					class_counter[i] += 1
				#print str(class_counter[i])
				#sys.exit()
		#print tmp_label_splitted
		#sys.exit()
	file_handle.close()

	for i in class_counter:
		print "\t\t\t\tClass " + str(i) + "\t" + str(class_counter[i])

	one_hot_samples = np.zeros((len(input_samples),input_sample_dim,4),dtype=np.float32)
	if input_mode == "RNAfold":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,3),dtype=np.float32)
	elif input_mode == "conservation":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,1),dtype=np.float32)

	one_hot_labels = np.zeros((len(input_sample_labels),input_sample_label_dim),dtype=np.float32)

	for row_num in range(len(input_samples)):

		input_sample = input_samples[row_num]
		#print input_sample

		if input_mode == "conservation":

			input_sample_splitted = input_sample.split("_")

			for col_num in range(len(input_sample_splitted)):
				one_hot_samples[row_num,col_num,0] = np.float32(input_sample_splitted[col_num])

		else:

			for col_num in range(len(input_sample)):

				if input_mode == "sequence":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_sequence(input_sample[col_num])
				elif input_mode == "RNAfold":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_RNAfold(input_sample[col_num])

		tmp_label = input_sample_labels[row_num]
		one_hot_labels[row_num,:] = np.asarray(tmp_label.split("_"),dtype=np.float32)

		#print input_sample
		#print one_hot_samples[row_num,:,:]
		#print current_sequence_minus

		#print tmp_label
		#print one_hot_labels[row_num,:]
		#sys.exit()

	return one_hot_samples, one_hot_labels, input_sample_label_dim, input_sample_dim


def file_sample_to_one_hot_predict(input_file, input_mode):
	"""
	Takes a tabular 2 column file as input and converts
	to the appropriate one-hot vector representation.
	"""

	input_samples = []
	input_sample_dim = 0
	file_handle = gzip.open(input_file,'r')
	for line in file_handle:

		line = line.strip("\n")
		split_line = line.split()

		input_samples.append(split_line[1])

		if input_sample_dim == 0:

			if input_mode == "conservation":
				tmp_input_sample = split_line[1]
				tmp_input_sample_splitted = tmp_input_sample.split("_")
				input_sample_dim = len(tmp_input_sample_splitted)
			else:
				input_sample_dim = len(split_line[1])
	file_handle.close()

	one_hot_samples = np.zeros((len(input_samples),input_sample_dim,4),dtype=np.float32)
	if input_mode == "RNAfold":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,3),dtype=np.float32)
	elif input_mode == "conservation":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,1),dtype=np.float32)

	for row_num in range(len(input_samples)):

		input_sample = input_samples[row_num]

		if input_mode == "conservation":

			input_sample_splitted = input_sample.split("_")

			for col_num in range(len(input_sample_splitted)):
				one_hot_samples[row_num,col_num,0] = np.float32(input_sample_splitted[col_num])

		else:

			for col_num in range(len(input_sample)):

				if input_mode == "sequence":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_sequence(input_sample[col_num])
				elif input_mode == "RNAfold":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_RNAfold(input_sample[col_num])

	return one_hot_samples


def batch_sample_to_one_hot(input_samples, input_sample_dim, input_mode):
	"""
	Takes a batch of samples as input and
	converts it to the appropriate one-hot vector
	representation.
	"""

	one_hot_samples = np.zeros((len(input_samples),input_sample_dim,4),dtype=np.float32)
	if input_mode == "RNAfold":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,3),dtype=np.float32)
	elif input_mode == "conservation":
		one_hot_samples = np.zeros((len(input_samples),input_sample_dim,1),dtype=np.float32)

	for row_num in range(len(input_samples)):

		line = input_samples[row_num]
		line = line.strip("\n")
		split_line = line.split()

		#print split_line[0]
		#print split_line[1]
		#sys.exit()

		input_sample = split_line[1]

		if input_mode == "conservation":

			input_sample_splitted = input_sample.split("_")

			for col_num in range(len(input_sample_splitted)):
				one_hot_samples[row_num,col_num,0] = np.float32(input_sample_splitted[col_num])

		else:

			for col_num in range(len(input_sample)):

				if input_mode == "sequence":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_sequence(input_sample[col_num])
				elif input_mode == "RNAfold":
					one_hot_samples[row_num,col_num,:] = onehot_conversion_RNAfold(input_sample[col_num])

	return one_hot_samples


def onehot_conversion_sequence(letter):

	one_hot_map = {
		"A": np.asarray([1, 0, 0, 0],dtype=np.float32), "a": np.asarray([1, 0, 0, 0],dtype=np.float32),
		"C": np.asarray([0, 1, 0, 0],dtype=np.float32), "c": np.asarray([0, 1, 0, 0],dtype=np.float32),
		"G": np.asarray([0, 0, 1, 0],dtype=np.float32), "g": np.asarray([0, 0, 1, 0],dtype=np.float32),
		"T": np.asarray([0, 0, 0, 1],dtype=np.float32), "t": np.asarray([0, 0, 0, 1],dtype=np.float32),
		"N": np.asarray([0, 0, 0, 0],dtype=np.float32), "n": np.asarray([0, 0, 0, 0],dtype=np.float32)}

	return one_hot_map[letter]


def onehot_conversion_RNAfold(letter):

	one_hot_map = {
		".": np.asarray([1, 0, 0],dtype=np.float32),
		"(": np.asarray([0, 1, 0],dtype=np.float32),
		")": np.asarray([0, 0, 1],dtype=np.float32)}

	return one_hot_map[letter]


def reverse_complement(input_sequence):

	complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
	return ''.join([complement[base] for base in input_sequence[::-1]])
