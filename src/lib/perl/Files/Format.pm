package Files::Format;

use warnings;
use strict;

use Parallel::ForkManager;
use Set::IntervalTree;

use lib "$FindBin::Bin/../../perl";
use Generic::FuS;
use Files::CleanUp;

sub extract_target_sequences {

	my ($chrom_list, $interval_file, $genome_file, $window_size, $step_size, $working_dir, $static_predict_flag, $input_mode, $conservation_dir, $thread_num) = @_;

	my %valid_chroms;
	if( $chrom_list ne "all" ){

		%valid_chroms = map { $_ => 1 } split(/,/, $chrom_list);
	}

	my (%regions, %true_chr_list);
	open(IN,$interval_file) or die "$!: $interval_file.\n";
	while(my $line = <IN>){
		chomp $line;

		my @temp = split(/\t/,$line);

# 		unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
# 			next;
# 		}

		if( $chrom_list ne "all" and !exists $valid_chroms{$temp[0]} ){
			next;
		}

		push @{$regions{$temp[0]}{$temp[5]}},\@temp;

		$true_chr_list{$temp[0]} = 1;
	}
	close IN;

	system "mkdir $working_dir" unless -d "$working_dir";

	foreach my $chr (sort { $a cmp $b } keys(%regions)){

		if( -e "$working_dir/targets.$chr.bed.gz" ){
			next;
		}

		warn "\t\t\tOn $chr.\n";

		open(OUT,">:gzip","$working_dir/targets.$chr.bed.gz") or die "$!: $working_dir/targets.$chr.bed.gz.\n";

		my $total_number_of_basepairs;
		foreach my $strand (sort { $a cmp $b } keys(%{$regions{$chr}})){

			@{$regions{$chr}{$strand}} = sort { $$a[1] <=> $$b[1] } @{$regions{$chr}{$strand}};

			for(my $j = 0; $j < @{$regions{$chr}{$strand}}; $j++){

				my @temp = @{${$regions{$chr}{$strand}}[$j]};

				if( $static_predict_flag == 1 ){

					my $start = int( ($temp[1] + $temp[2]) / 2 ) - int( $window_size / 2 );
					my $stop = int( ($temp[1] + $temp[2]) / 2 ) + int( $window_size / 2 );

					print OUT "$temp[0]\t$start\t$stop\t$temp[3]\t0\t$temp[5]\n";

					$total_number_of_basepairs += $stop - $start;
				}
				else{

					if( $temp[2] - $temp[1] < $window_size ){
						next;
					}

					for(my $i = $temp[1] + int( $window_size / 2 ); $i <= $temp[2] - int( $window_size / 2 ); $i += $step_size){

						my $window_start = $i - int( $window_size / 2 );
						my $window_stop = $i + int( $window_size / 2 );
						my $window_name = $temp[3]."_window_".($i - $temp[1] - int( $window_size / 2 ));

						print OUT "$temp[0]\t$window_start\t$window_stop\t$window_name\t0\t$temp[5]\n";

						$total_number_of_basepairs += $window_stop - $window_start;
					}
				}
			}
		}
		close OUT;

		my $time = time();
		system "bedtools getfasta -name -s -tab -fi $genome_file -bed $working_dir/targets.$chr.bed.gz -fo $working_dir/targets.$chr.sequence.tab";
		system "gzip $working_dir/targets.$chr.sequence.tab";
		warn "\t\t\t\t$total_number_of_basepairs nts extracted in ".(time() - $time)." seconds\n";

		system "bedtools getfasta -name -s -fi $genome_file -bed $working_dir/targets.$chr.bed.gz -fo $working_dir/targets.$chr.DNA.fa" if $input_mode =~ /RNAfold/;
		system "gzip $working_dir/targets.$chr.DNA.fa" if $input_mode =~ /RNAfold/;

		Files::CleanUp::finalize_files("$working_dir", "targets.$chr", $input_mode, $conservation_dir, $thread_num) unless $input_mode eq "sequence";
	}

	return join(",", sort { $a cmp $b } keys(%true_chr_list));
}

sub bedGraph_tracks_from_prediction_scan {

	my ($working_dir, $step_size, $chrom_list, $class_num, $input_mode, $model_dir_name, $interm_dir) = @_;

	system "mkdir $working_dir/$model_dir_name/bedGraph_tracks" unless -d "$working_dir/$model_dir_name/bedGraph_tracks";

	$input_mode =~ s/,/\./g;

	my @chroms = split(/,/,$chrom_list);
	foreach my $chrom (@chroms){

		for(my $i = 0; $i < $class_num; $i++){

			warn "\t\t\tOn $chrom. Preparing files for class $i.\n";

			my @class_specific_preds;
			open(PREDS,"<:gzip","$working_dir/$model_dir_name/intermediate_files/targets.$chrom.predictions.txt.gz") or die "$!: $working_dir/$model_dir_name/intermediate_files/targets.$chrom.predictions.txt.gz.\n";
			while( !eof(PREDS) ){

				my $line_sense = <PREDS>;

				my @temp_sense = split(/\t/,$line_sense);

				push @class_specific_preds, $temp_sense[$i];
			}
			close PREDS;

			my $line_counter = 0;
			my $class_specific_color = Generic::FuS::random_RGB();
			open(BEDGRAPH, ">:gzip", "$working_dir/$model_dir_name/bedGraph_tracks/predictions.$chrom.class_$i.bedGraph.gz") or die "$!: $working_dir/$model_dir_name/bedGraph_tracks/predictions.$chrom.class_$i.bedGraph.gz.\n";
			print BEDGRAPH "track type=bedGraph name=\"$input_mode\_class.$i\" description=$input_mode\_class.$i visibility=full color=$class_specific_color autoScale=off viewLimits=-1:1\n";
			open(BED,">:gzip","$working_dir/$model_dir_name/bedGraph_tracks/predictions.$chrom.class_$i.bed.gz") or die "$!: $working_dir/$model_dir_name/bedGraph_tracks/predictions.$chrom.class_$i.bed.gz.\n";
			open(IN,"<:gzip","$interm_dir/targets.$chrom.bed.gz") or die "$!: $interm_dir/targets.$chrom.bed.gz.\n";
			while(my $line = <IN>){

				chomp $line;

				my @temp = split(/\t/,$line);

				print BED "$temp[0]\t$temp[1]\t$temp[2]\t$temp[3]\t$class_specific_preds[$line_counter]\t$temp[5]\n";

				my $start = int(($temp[1] + $temp[2]) / 2) - int($step_size / 2);
				my $stop = int(($temp[1] + $temp[2]) / 2) + int($step_size / 2);

				if( $temp[5] eq "+" ){
					print BEDGRAPH "$temp[0]\t$start\t$stop\t$class_specific_preds[$line_counter]\n";
				}
				else{
					print BEDGRAPH "$temp[0]\t$start\t$stop\t-$class_specific_preds[$line_counter]\n";
				}

				$line_counter++;
			}
			close IN;
			close BEDGRAPH;
			close BED;
		}
	}

	for(my $i = 0; $i < $class_num; $i++){

		system "zcat $working_dir/$model_dir_name/bedGraph_tracks/predictions.*.class_$i.bedGraph.gz | gzip --fast > $working_dir/$model_dir_name/bedGraph_tracks/all.predictions.class_$i.bedGraph.gz" unless scalar(@chroms) == 1;

		system "zcat $working_dir/$model_dir_name/bedGraph_tracks/predictions.*.class_$i.bed.gz | gzip --fast > $working_dir/$model_dir_name/bedGraph_tracks/all.predictions.class_$i.bed.gz" unless scalar(@chroms) == 1;
	}
}

sub update_bed_with_predictions {

	my ($working_dir, $chrom_list, $class_num, $model_dir_name, $interm_dir) = @_;

	system "mkdir $working_dir/$model_dir_name/bed_tracks" unless -d "$working_dir/$model_dir_name/bed_tracks";

	my @chroms = split(/,/,$chrom_list);
	foreach my $chrom (@chroms){

		for(my $i = 0; $i < $class_num; $i++){

			warn "\t\t\tOn $chrom. Preparing files for class $i.\n";

			my @class_specific_preds;
			open(PREDS,"<:gzip","$working_dir/$model_dir_name/intermediate_files/targets.$chrom.predictions.txt.gz") or die "$!: $working_dir/$model_dir_name/intermediate_files/targets.$chrom.predictions.txt.gz.\n";
			while( !eof(PREDS) ){

				my $line_sense = <PREDS>;

				my @temp_sense = split(/\t/,$line_sense);

				push @class_specific_preds, $temp_sense[$i];
			}
			close PREDS;

			my $line_counter = 0;
			open(BED,">:gzip","$working_dir/$model_dir_name/bed_tracks/predictions.$chrom.class_$i.bed.gz") or die "$!: $working_dir/$model_dir_name/bed_tracks/predictions.$chrom.class_$i.bed.gz.\n";
			open(IN,"<:gzip","$interm_dir/targets.$chrom.bed.gz") or die "$!: $interm_dir/targets.$chrom.bed.gz.\n";
			while(my $line = <IN>){

				chomp $line;

				my @temp = split(/\t/,$line);

				print BED "$temp[0]\t$temp[1]\t$temp[2]\t$temp[3]\t$class_specific_preds[$line_counter]\t$temp[5]\n";

				$line_counter++;
			}
			close IN;
			close BED;
		}
	}

	for(my $i = 0; $i < $class_num; $i++){

		system "zcat $working_dir/$model_dir_name/bed_tracks/predictions.*.class_$i.bed.gz | gzip --fast > $working_dir/$model_dir_name/bed_tracks/all.predictions.class_$i.bed.gz" unless scalar(@chroms) == 1;
	}
}

1;
