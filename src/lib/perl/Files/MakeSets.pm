package Files::MakeSets;

use warnings;
use strict;

use Set::IntervalTree;
use PerlIO::gzip;

sub make_sets {

	my ($peak_list, $class_list, $chrom_list_test, $chrom_list_valid, $size, $working_dir, $extend_flag, $reinforced_number) = @_;

	if( $class_list eq "auto" ){
		make_sets_auto_labels($peak_list, $chrom_list_test, $chrom_list_valid, $size, $working_dir, $extend_flag, $reinforced_number);
	}
	else{
		make_sets_fixed_labels($peak_list, $class_list, $chrom_list_test, $chrom_list_valid, $size, $working_dir, $extend_flag, $reinforced_number);
	}

}

sub make_sets_fixed_labels {

	my ($peak_list, $class_list, $chrom_list_test, $chrom_list_valid, $size, $working_dir, $extend_flag, $reinforced_number) = @_;

	# Splitting comma separated parameters into arrays
	my @peaks = split(/,/, $peak_list);
	my @classes = split(/,/, $class_list);
	my @test_chroms = split(/,/, $chrom_list_test);
	my @valid_chroms = split(/,/, $chrom_list_valid);

	# Loading test and validation chromosomes into a hash structure for easily separating peaks later on
	my %blacklist_chr;
	foreach my $chr (@test_chroms){

		$blacklist_chr{"test"}{$chr} = 1;
	}

	foreach my $chr (@valid_chroms){

		$blacklist_chr{"valid"}{$chr} = 1;
	}

	# Generating one hot encoded class representation based on the provided class list. The returned value is a hash reference
	my $one_hot_classes = class_one_hot_generator_fixed(\@classes);

	# Loading peaks and assigning the one hot represented classes to peaks in each file
	open(TEST,">:gzip","$working_dir/test.bed.gz") or die "$!: $working_dir/test.bed.gz.\n";
	open(TRAIN,">:gzip","$working_dir/train.bed.gz") or die "$!: $working_dir/train.bed.gz.\n";
	open(VALID,">:gzip","$working_dir/validation.bed.gz") or die "$!: $working_dir/validation.bed.gz.\n";
	my (%labels);
	for(my $i = 0; $i < @peaks; $i++){

		my $name = "file_".($i + 1);

		$labels{$name} = $$one_hot_classes{$classes[$i]};
# 		die "$labels{$name}\n";

		open(IN,$peaks[$i]) or die "$!: $peaks[$i].\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

# 			unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
# 				next;
# 			}

			if( $temp[2] - $temp[1] > $size ){
				next;
			}

			for( my $j = 1; $j <= $reinforced_number; $j++){

				my $new_start = $temp[1];
				my $new_stop = $temp[2];

				if( $extend_flag == 1 ){

					my $missing_space = $size - ($temp[2] - $temp[1]);
					my $random_spot = int(rand($missing_space));

					$new_start = $temp[1] - $random_spot;
					$new_stop = $temp[2] + ($missing_space - $random_spot);
				}

				# Add one hot class label as a 5th column in each bed file
				$temp[4] = $labels{$name};

				my @tmp = ($temp[0], $new_start, $new_stop, "$temp[3]_reinforced$j", $temp[4], $temp[5]);

				if( exists $blacklist_chr{"test"}{$temp[0]} ){

					print TEST join("\t", @tmp)."\n";
				}
				elsif( exists $blacklist_chr{"valid"}{$temp[0]} ){

					print VALID join("\t", @tmp)."\n";
				}
				else{

					print TRAIN join("\t", @tmp)."\n";
				}
			}
		}
		close IN;
	}
	close TEST;
	close TRAIN;
	close VALID;
}

sub make_sets_auto_labels {

	my ($peak_list, $chrom_list_test, $chrom_list_valid, $size, $working_dir, $extend_flag, $reinforced_number) = @_;

	# Splitting comma separated parameters into arrays
	my @peaks = split(/,/, $peak_list);
	my @test_chroms = split(/,/, $chrom_list_test);
	my @valid_chroms = split(/,/, $chrom_list_valid);

	# Loading test and validation chromosomes into a hash structure for easily separating peaks later on
	my %blacklist_chr;
	foreach my $chr (@test_chroms){

		$blacklist_chr{"test"}{$chr} = 1;
	}

	foreach my $chr (@valid_chroms){

		$blacklist_chr{"valid"}{$chr} = 1;
	}

	# Generating one hot encoded class representation based on the provided class list. The returned value is a hash reference
	my $one_hot_classes = class_one_hot_generator_auto(\@peaks, $size, $extend_flag);

	# Loading peaks and assigning the one hot represented classes to peaks in each file
	open(TEST,">:gzip","$working_dir/test.bed.gz") or die "$!: $working_dir/test.bed.gz.\n";
	open(TRAIN,">:gzip","$working_dir/train.bed.gz") or die "$!: $working_dir/train.bed.gz.\n";
	open(VALID,">:gzip","$working_dir/validation.bed.gz") or die "$!: $working_dir/validation.bed.gz.\n";
	for(my $i = 0; $i < @peaks; $i++){

		open(IN,$peaks[$i]) or die "$!: $peaks[$i].\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

# 			unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
# 				next;
# 			}

			if( $temp[2] - $temp[1] > $size ){
				next;
			}

# 			die "$temp[3]\t".$$one_hot_classes{$temp[3]}."\n";

			for( my $j = 1; $j <= $reinforced_number; $j++){

				my $new_start = $temp[1];
				my $new_stop = $temp[2];

				if( $extend_flag == 1 ){

					my $missing_space = $size - ($temp[2] - $temp[1]);
					my $random_spot = int(rand($missing_space));

					$new_start = $temp[1] - $random_spot;
					$new_stop = $temp[2] + ($missing_space - $random_spot);
				}

				my @tmp = ($temp[0], $new_start, $new_stop, "$temp[3]_reinforced$j", $$one_hot_classes{$temp[3]}, $temp[5]);

				if( exists $blacklist_chr{"test"}{$temp[0]} ){

					print TEST join("\t", @tmp)."\n";
				}
				elsif( exists $blacklist_chr{"valid"}{$temp[0]} ){

					print VALID join("\t", @tmp)."\n";
				}
				else{

					print TRAIN join("\t", @tmp)."\n";
				}
			}
		}
		close IN;
	}
	close TEST;
	close TRAIN;
	close VALID;
}

sub class_one_hot_generator_fixed {

	my ($classes_ref) = @_;

	my %classes;
	for(my $i = 0; $i < @$classes_ref; $i++){

		$classes{$$classes_ref[$i]} = 1;
	}

	my @one_hot_classes;
	for(my $i = 0; $i < scalar(keys(%classes)); $i++){

		for(my $j = 0; $j < scalar(keys(%classes)); $j++){

			if( $i == $j ){

				$one_hot_classes[$i][$j] = 1;
			}
			else{

				$one_hot_classes[$i][$j] = 0;
			}
		}
	}

	foreach my $class_number (sort { $a <=> $b } keys %classes){

# 		warn "$class_number\t".join("_", @{$one_hot_classes[$class_number - 1]})."\n";
		$classes{$class_number} = join("_", @{$one_hot_classes[$class_number - 1]});
	}
# 	die;

	return \%classes;
}

sub class_one_hot_generator_auto {

	my ($peaks_ref, $size, $extend_flag) = @_;

	# Loop through region files, extend intervals and add them to an IntervalTree structure
	my (%regions_IT);
	for(my $i = 0; $i < @$peaks_ref; $i++){

		open(IN,$$peaks_ref[$i]) or die "$!: $$peaks_ref[$i]\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

# 			unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
# 				next;
# 			}

			unless( exists $regions_IT{$i}{$temp[0]}{$temp[5]} ){
				$regions_IT{$i}{$temp[0]}{$temp[5]} = Set::IntervalTree->new;
			}

			if( $extend_flag == 1 ){

				my $start = int(($temp[1] + $temp[2]) / 2) - int($size / 2);
				my $stop = int(($temp[1] + $temp[2]) / 2) + int($size / 2);

				$temp[1] = $start;
				$temp[2] = $stop;
			}

			$regions_IT{$i}{$temp[0]}{$temp[5]}->insert($temp[3],$temp[1],$temp[2]);
		}
		close IN;
	}

	# Loop through region files, extend intervals and find overlap with other region files to create the multi-class multi-label target vector
	my %one_hot_classes;
	for(my $i = 0; $i < @$peaks_ref; $i++){

		open(IN,$$peaks_ref[$i]) or die "$!: $$peaks_ref[$i]\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

# 			unless( $temp[0] =~ /^chr(\d{1,2}|X)$/ ){
# 				next;
# 			}

			if( $extend_flag == 1 ){

				my $start = int(($temp[1] + $temp[2]) / 2) - int($size / 2);
				my $stop = int(($temp[1] + $temp[2]) / 2) + int($size / 2);

				$temp[1] = $start;
				$temp[2] = $stop;
			}

			my @tmp_labels = (0) x scalar(@$peaks_ref);
			$tmp_labels[$i] = 1;
			for(my $j = 0; $j < @$peaks_ref; $j++){

				if( $i == $j ){
					next;
				}

				unless( exists $regions_IT{$j}{$temp[0]}{$temp[5]} ){
					next;
				}

				my $results = $regions_IT{$j}{$temp[0]}{$temp[5]}->fetch($temp[1],$temp[2]);
				if( scalar(@$results) >= 1 ){

					$tmp_labels[$j] = 1;
				}
			}

# 			die "$temp[3]\t".join("\t",@tmp_labels)."\n";

			$one_hot_classes{$temp[3]} = join("_",@tmp_labels);
# 			die "$temp[3]\t".$one_hot_classes{$temp[3]}."\n";
		}
		close IN;
	}

	return \%one_hot_classes;
}

1;
