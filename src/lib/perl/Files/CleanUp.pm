package Files::CleanUp;

use PerlIO::gzip;

use FindBin;
use lib "$FindBin::Bin/../../../perl/";
use Generic::FuS;

use warnings;
use strict;

sub finalize_sequence_files {

	my ($working_dir, $file_list, $genome_file, $max_size, $extend_flag, $shuffled_class_flag, $num_classes) = @_;

	if( $shuffled_class_flag == 0 ){

		my @files = split(/,/, $file_list);
		foreach my $file (@files){

			system "bedtools getfasta -s -tab -name -fi $genome_file -bed $working_dir/$file.bed.gz -fo $working_dir/$file.DNA.tab";
			system "gzip $working_dir/$file.DNA.tab";

			my %label_map;
			open(IN,"<:gzip","$working_dir/$file.bed.gz") or die "$!: $working_dir/$file.bed.gz.\n";
			while(my $line = <IN>){
				chomp $line;

				my @temp = split(/\t/,$line);

				@{$label_map{$temp[3]}} = @temp;
			}
			close IN;

			open(OUT,">:gzip","$working_dir/$file.sequence.tab.gz") or die "$!: $working_dir/$file.sequence.tab.gz.\n";
			open(FA,">:gzip","$working_dir/$file.DNA.fa.gz") or die "$!: $working_dir/$file.DNA.fa.gz.\n";
			open(IN,"<:gzip","$working_dir/$file.DNA.tab.gz") or die "$!: $working_dir/$file.DNA.tab.gz.\n";
			while(my $line = <IN>){
				chomp $line;

				my @temp = split(/\t/,$line);

				$temp[0] =~ s/\((\+|-)\)//g;
				my $strand = $1;

				$temp[1] = uc($temp[1]);

				my $tmp_chrom = ${$label_map{$temp[0]}}[0];
				my $tmp_start = ${$label_map{$temp[0]}}[1];
				my $tmp_stop = ${$label_map{$temp[0]}}[2];
				my $tmp_label = ${$label_map{$temp[0]}}[4];

				if( $extend_flag == 1 ){

					print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\t$temp[1]\t$strand\n";
					print FA ">$temp[0]\n$temp[1]\n";
				}
				else{

					my $main_seq = $temp[1];

					if( $max_size > length($temp[1]) ){

						my $diff = $max_size - length($temp[1]);
						my $random_spot = int(rand($diff));

						my $left_padding_size = $random_spot;
						my $right_padding_size = $diff - $random_spot;

						my @left_paddings = ("N") x $left_padding_size;
						my @right_paddings = ("N") x $right_padding_size;

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\t".join("",@left_paddings).$main_seq.join("",@right_paddings)."\t$strand\n";
						print FA ">$temp[0]\n".join("",@left_paddings).$main_seq.join("",@right_paddings)."\n";
					}
					else{

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\t$main_seq\t$strand\n";
						print FA ">$temp[0]\n$main_seq\n";
					}
				}
			}
			close OUT;
			close IN;
			close FA;
		}
	}
	else{

		my @files = split(/,/, $file_list);
# 		die $num_classes;

		my @shuffled_labels = (0) x $num_classes;
		push @shuffled_labels, 1;
		my $shuffled_labels_str = join("_", @shuffled_labels);

		foreach my $file (@files){

			system "bedtools getfasta -s -name -fi $genome_file -bed $working_dir/$file.bed.gz -fo $working_dir/$file.DNA.fa";

			system "fasta-shuffle-letters -seed 1984 -kmer 2 -line $max_size -tag shuffled $working_dir/$file.DNA.fa $working_dir/$file.DNA.shuffled.fa";

			system "gzip $working_dir/$file.DNA.fa";
			system "gzip $working_dir/$file.DNA.shuffled.fa";

			my (%shuffled_seq,$header);
			open(IN,"<:gzip","$working_dir/$file.DNA.shuffled.fa.gz") or die "$!: $working_dir/$file.DNA.shuffled.fa.gz\n";
			while(my $line = <IN>){
				chomp $line;

				if( $line =~ />(.+)\((\+|-)\)shuffled/ ){

					$header = $1;
				}
				else{

					$shuffled_seq{$header}{"name"} = $header."shuffled";
					$shuffled_seq{$header}{"seq"} .= $line;
				}
			}
			close IN;

			system "bedtools getfasta -s -name -tab -fi $genome_file -bed $working_dir/$file.bed.gz -fo $working_dir/$file.DNA.tab";
			system "gzip $working_dir/$file.DNA.tab";

			my %label_map;
			open(IN,"<:gzip","$working_dir/$file.bed.gz") or die "$!: $working_dir/$file.bed.gz.\n";
			while(my $line = <IN>){
				chomp $line;

				my @temp = split(/\t/,$line);

				@{$label_map{$temp[3]}} = @temp;
			}
			close IN;

			open(OUT,">:gzip","$working_dir/$file.sequence.tab.gz") or die "$!: $working_dir/$file.sequence.tab.gz.\n";
			open(FA,">:gzip","$working_dir/$file.DNA.fa.gz") or die "$!: $working_dir/$file.DNA.fa.gz.\n";
			open(IN,"<:gzip","$working_dir/$file.DNA.tab.gz") or die "$!: $working_dir/$file.DNA.tab.gz.\n";
			while(my $line = <IN>){
				chomp $line;

				my @temp = split(/\t/,$line);

				$temp[0] =~ s/\((\+|-)\)//g;
				my $strand = $1;

				$temp[1] = uc($temp[1]);

				my $tmp_chrom = ${$label_map{$temp[0]}}[0];
				my $tmp_start = ${$label_map{$temp[0]}}[1];
				my $tmp_stop = ${$label_map{$temp[0]}}[2];
				my $tmp_label = ${$label_map{$temp[0]}}[4];

				my $shuffled_seq_name = $shuffled_seq{$temp[0]}{"name"};
				my $shuffled_seq_seq = uc($shuffled_seq{$temp[0]}{"seq"});

				if( $extend_flag == 1 ){

					print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\_0\t$temp[1]\t$strand\n";
					print FA ">$temp[0]\n$temp[1]\n";

					print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$shuffled_seq_name\t$shuffled_labels_str\t$shuffled_seq_seq\t$strand\n";
					print FA ">$shuffled_seq_name\n$shuffled_seq_seq\n";
				}
				else{

					my $main_seq = $temp[1];

					my $main_seq_shuffled = $shuffled_seq_seq;

					if( $max_size > length($temp[1]) ){

						my $diff = $max_size - length($temp[1]);
						my $random_spot = int(rand($diff));

						my $left_padding_size = $random_spot;
						my $right_padding_size = $diff - $random_spot;

						my @left_paddings = ("N") x $left_padding_size;
						my @right_paddings = ("N") x $right_padding_size;

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\_0\t".join("",@left_paddings).$main_seq.join("",@right_paddings)."\t$strand\n";
						print FA ">$temp[0]\n".join("",@left_paddings).$main_seq.join("",@right_paddings)."\n";

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$shuffled_seq_name\t$shuffled_labels_str\t".join("",@left_paddings).$main_seq_shuffled.join("",@right_paddings)."\t$strand\n";
						print FA ">$shuffled_seq_name\n".join("",@left_paddings).$main_seq_shuffled.join("",@right_paddings)."\n";
					}
					else{

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$temp[0]\t$tmp_label\_0\t$main_seq\t$strand\n";
						print FA ">$temp[0]\n$main_seq\n";

						print OUT "$tmp_chrom\t$tmp_start\t$tmp_stop\t$shuffled_seq_name\t$shuffled_labels_str\t$main_seq_shuffled\t$strand\n";
						print FA ">$shuffled_seq_name\n$main_seq_shuffled\n";
					}
				}
			}
			close OUT;
			close IN;
			close FA;
		}
	}
}

sub finalize_fold_files {

	my ($working_dir, $file_list, $thread_num) = @_;

	my @files = split(/,/, $file_list);
	foreach my $file (@files){

		open(OUT,">:gzip","$working_dir/$file.RNA.fa.gz") or die "$!: $working_dir/$file.RNA.fa.gz.\n";
		open(IN,"<:gzip","$working_dir/$file.DNA.fa.gz") or die "$!: $working_dir/$file.DNA.fa.gz.\n";
		while(my $line = <IN>){
			chomp $line;

			if( $line =~ />/ ){

				print OUT "$line\n";
			}
			else{

				print OUT Generic::FuS::dna_to_rna($line)."\n";
			}
		}
		close IN;
		close OUT;

		my $time = time();
		system "zcat $working_dir/$file.RNA.fa.gz | RNAfold --noPS --noconv --jobs=$thread_num > $working_dir/$file.RNAfold.fa";
		system "gzip $working_dir/$file.RNAfold.fa";
		warn "\t\t\t\t$file - RNAfold calculation in ".(time() - $time)." seconds\n";

		my (%data,$header);
		open(IN,"<:gzip","$working_dir/$file.RNAfold.fa.gz") or die "$!: $working_dir/$file.RNAfold.fa.gz.\n";
		while(my $line = <IN>){
			chomp $line;

			if( $line =~ /^>(.+)$/ ){

				$header = $1;
			}
			else{

				if( $line =~ /^(.+?)\s+\(/ ){

					$data{$header} = $1;
				}
			}
		}
		close IN;

		open(OUT,">:gzip","$working_dir/$file.RNAfold.tab.gz") or die "$!: $working_dir/$file.RNAfold.tab.gz.\n";
		open(IN,"<:gzip","$working_dir/$file.sequence.tab.gz") or die "$!: $working_dir/$file.sequence.tab.gz.\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

			if( scalar(@temp) > 2 ){

				$temp[5] = $data{$temp[3]};

				print OUT join("\t",@temp)."\n";
			}
			else{

				$temp[1] = $data{$temp[0]};

				print OUT join("\t",@temp)."\n";
			}
		}
		close OUT;
		close IN;
	}
}

sub finalize_conservation_files {

	my ($working_dir, $file_list, $conservation_dir) = @_;

	my @files = split(/,/, $file_list);
	foreach my $file (@files){

		open(IN,"<:gzip","$working_dir/$file.sequence.tab.gz") or die "$!: $working_dir/$file.sequence.tab.gz.\n";
		my $tmp_line = <IN>;
		my @tmp_line_array = split(/\t/,$tmp_line);
		my $type_input_file = scalar(@tmp_line_array);
		close IN;

		my %regions_nt_res;
		open(IN,"<:gzip","$working_dir/$file.bed.gz") or die "$!: $working_dir/$file.bed.gz.\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);

			for(my $i = $temp[1]; $i < $temp[2]; $i++){

				$regions_nt_res{$temp[0]}{$i} = 0;
			}
		}
		close IN;

		my $time = time();
		if( -d $conservation_dir ){

			foreach my $chrom (keys %regions_nt_res){

				my ($current_position, $current_step);
				open(IN,"<:gzip","$conservation_dir/$chrom.wigFix.gz") or die "$!: $conservation_dir/$chrom.wigFix.gz.\n";
				while(my $line = <IN>){
					chomp $line;

# 					fixedStep chrom=chr1 start=10918 step=1
# 					0.064

					if( $line =~ /^fixedStep\schrom=.+?\sstart=(.+?)\sstep=(.+)$/ ){

						$current_position = $1 - 1;
						$current_step = $2;
					}
					else{

						$current_position += $current_step;

						if( exists $regions_nt_res{$chrom}{$current_position} ){

							$regions_nt_res{$chrom}{$current_position} = $line;
						}
					}
				}
				close IN;
			}
		}
		elsif( $conservation_dir =~ /\.json$/ ){

			open(IN,$conservation_dir) or die "$!: $conservation_dir\n";
			while(my $line = <IN>){
				chomp $line;

				unless( $line =~ /^".+".+\[/ ){
					next;
				}

				$line =~ /^"(.+)".+\[(.+)\]/;
				my $chrom = $1;
				my $conservation_values = $2;
				my @values = split(/,/, $conservation_values);

				for(my $i = 0; $i < @values; $i++){

					if( exists $regions_nt_res{$chrom}{$i} ){

						$regions_nt_res{$chrom}{$i} = $values[$i] / 1000;
					}
				}
			}
		}

		warn "\t\t\t\t$file - conservation calculation in ".(time() - $time)." seconds\n";

		open(OUT,">:gzip","$working_dir/$file.conservation.tab.gz") or die "$!: $working_dir/$file.conservation.tab.gz.\n";
		open(IN,"<:gzip","$working_dir/$file.bed.gz") or die "$!: $working_dir/$file.bed.gz.\n";
		while(my $line = <IN>){
			chomp $line;

			my @temp = split(/\t/,$line);
			my $strand = $temp[5];

			my @tmp_score;
			for(my $i = $temp[1]; $i < $temp[2]; $i++){

				push @tmp_score, $regions_nt_res{$temp[0]}{$i};
			}

			if( $strand eq "-" ){
				@tmp_score = reverse(@tmp_score);
			}

			if( $type_input_file > 2 ){

				$temp[5] = join("_", @tmp_score);

				print OUT join("\t", @temp)."\t$strand\n";
			}
			else{

				print OUT "$temp[3]\t".join("_", @tmp_score)."\n";
			}
		}
		close IN;
		close OUT;
	}
}

sub finalize_files {

	my ($working_dir, $file_list, $input_mode, $conservation_dir, $thread_num) = @_;

	finalize_fold_files($working_dir, $file_list, $thread_num) if $input_mode =~ /RNAfold/;
	finalize_conservation_files($working_dir, $file_list, $conservation_dir) if $input_mode =~ /conservation/;
}

1;
