package Models::DNN;

use warnings;
use strict;

use FindBin;
use lib "$FindBin::Bin/../";
use Files::Format;

sub train_DNN {

	my ($input_dir, $output_dir, $exec_dir, $input_mode, $model_type) = @_;

	my %architectures = (
		"CNN" => "$exec_dir/train_CNN.py",
		"CAE" => "$exec_dir/train_CAE.py",
		"CVAE" => "$exec_dir/train_CVAE.py"
	);

	unless( exists $architectures{$model_type} ){
		die "\n\t\tERROR: Architecture $model_type not supported!\n";
	}

	my $tmp_cmd = "python $architectures{$model_type} $input_dir $output_dir $input_mode";
	system $tmp_cmd;
}

sub predict_targets {

	my ($chrom_list, $working_dir, $interm_dir, $exec_dir, $model_file, $static_predict_flag, $class_num, $step_size, $input_mode, $model_dir_name, $model_type) = @_;

	if( $static_predict_flag == 1 ){

		system "mkdir $working_dir/static/$model_dir_name" unless -d "$working_dir/static/$model_dir_name";
		system "mkdir $working_dir/static/$model_dir_name/intermediate_files" unless -d "$working_dir/static/$model_dir_name/intermediate_files";

		predict_regions($chrom_list, "$working_dir/static", $exec_dir, $model_file, $input_mode, $model_dir_name, $model_type, $interm_dir);

		warn "\t\tParsing predictions and updating bed files.\n";
		Files::Format::update_bed_with_predictions("$working_dir/static", $chrom_list, $class_num, $model_dir_name, $interm_dir);
	}
	else{

		system "mkdir $working_dir/scan/$model_dir_name" unless -d "$working_dir/scan/$model_dir_name";
		system "mkdir $working_dir/scan/$model_dir_name/intermediate_files" unless -d "$working_dir/scan/$model_dir_name/intermediate_files";

		predict_regions($chrom_list, "$working_dir/scan", $exec_dir, $model_file, $input_mode, $model_dir_name, $model_type, $interm_dir);

		warn "\t\tParsing predictions and making bedGraph tracks.\n";
		Files::Format::bedGraph_tracks_from_prediction_scan("$working_dir/scan", $step_size, $chrom_list, $class_num, $input_mode, $model_dir_name, $interm_dir);
	}
}

sub predict_regions {

	my ($chrom_list, $working_dir, $exec_dir, $model_file, $input_mode, $model_dir_name, $model_type, $interm_dir) = @_;

	my %architectures = (
		"CNN" => "$exec_dir/apply_CNN.py",
		"CAE" => "$exec_dir/apply_CAE.py",
		"CVAE" => "$exec_dir/apply_CVAE.py"
	);

	unless( exists $architectures{$model_type} ){
		die "\n\t\tERROR: Architecture $model_type not supported!\n";
	}

	my @chroms = split(/,/, $chrom_list);
	foreach my $chr (@chroms){

		warn "\t\t\tOn $chr.\n";

		my $tmp_cmd = "python $architectures{$model_type} $model_file $interm_dir/targets.$chr $working_dir/$model_dir_name/intermediate_files/targets.$chr $input_mode";
		system "$tmp_cmd" unless -e "$working_dir/$model_dir_name/intermediate_files/targets.$chr.predictions.txt.gz";
	}
}

1;
