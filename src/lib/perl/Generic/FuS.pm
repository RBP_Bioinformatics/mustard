package Generic::FuS;

use warnings;
use strict;

sub random_RGB {

	return int(rand(255)).",".int(rand(255)).",".int(rand(255));
}

sub get_rc {

	my ($sequence) = @_;

	$sequence = reverse($sequence);
	$sequence =~ tr/ACGTNacgtn/TGCANtgcan/;

	return $sequence;
}

sub dna_to_rna {

	my ($sequence) = @_;

	$sequence =~ tr/Tt/Uu/;

	return $sequence;
}

1;
