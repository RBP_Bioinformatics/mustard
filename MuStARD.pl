#!/usr/bin/perl
use warnings;
use strict;

use Getopt::Long;
use FindBin;
use lib "$FindBin::Bin/src/lib/perl/";
use Generic::FuS;

usage() if @ARGV < 1 or (
	$ARGV[0] ne "train" and 
	$ARGV[0] ne "predict"
);

my $mode = shift(@ARGV);
my $arguments = join(" ", @ARGV);

system "perl $FindBin::Bin/src/MuStARD_train.pl $arguments" if $mode eq "train";

system "perl $FindBin::Bin/src/MuStARD_predict.pl $arguments" if $mode eq "predict";

sub usage {

	warn	"\nUnknown mode!\n";
	warn	"\nUsage: MuStARD.pl [Mode] [Options]\n".
		"\n\tSupported Modes:\n".
		"\ttrain\t\tMode for training a new model. Type MuStARD train -h|--help to browse more options for train mode.\n".
		"\tpredict\t\tMode for using an existing model on a specific list of genomic loci. Type MuStARD predict -h|--help to browse more options for predict mode.\n";
	exit;
}
